package com.qlhx.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.qlhx.model.Facedevice;

public interface FacedeviceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Facedevice record);

    int insertSelective(Facedevice record);

    Facedevice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Facedevice record);

    int updateByPrimaryKey(Facedevice record);

    List<Facedevice> getalldevicebymodel(@Param("model") String model);

    int updateDeviceOnlineStatus(Facedevice record);
    
    Facedevice selectByIp(String ip);
    Facedevice selectByIpandId(@Param("ip") String ip,@Param("id") String id);

    List<Facedevice> findAll();
   }