package com.qlhx.model;

import java.util.Date;

public class BaseMember {
    private Long id;

    private String nickname;

    private Integer sex;

    private Integer nation;

    private Date birthday;

    private String address;

    private String idnum;

    private String photo;

    private String issuing;

    private Date validitydatestart;

    private String validitydateend;

    private String phone;

    private Integer depid;

    private String telephone;

    private String ecardnum;

    private Date ecardendtime;

    private String pinyin;

    private String code;

    private Integer type;

    private String syncid;

    private Integer isbindfinger;

    private Integer isbindface;

    private String memberidentifier;

    private String groupidentifier;

    private String groupname;

    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getNation() {
        return nation;
    }

    public void setNation(Integer nation) {
        this.nation = nation;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum == null ? null : idnum.trim();
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    public String getIssuing() {
        return issuing;
    }

    public void setIssuing(String issuing) {
        this.issuing = issuing == null ? null : issuing.trim();
    }

    public Date getValiditydatestart() {
        return validitydatestart;
    }

    public void setValiditydatestart(Date validitydatestart) {
        this.validitydatestart = validitydatestart;
    }

    public String getValiditydateend() {
        return validitydateend;
    }

    public void setValiditydateend(String validitydateend) {
        this.validitydateend = validitydateend == null ? null : validitydateend.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getDepid() {
        return depid;
    }

    public void setDepid(Integer depid) {
        this.depid = depid;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public String getEcardnum() {
        return ecardnum;
    }

    public void setEcardnum(String ecardnum) {
        this.ecardnum = ecardnum == null ? null : ecardnum.trim();
    }

    public Date getEcardendtime() {
        return ecardendtime;
    }

    public void setEcardendtime(Date ecardendtime) {
        this.ecardendtime = ecardendtime;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin == null ? null : pinyin.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSyncid() {
        return syncid;
    }

    public void setSyncid(String syncid) {
        this.syncid = syncid == null ? null : syncid.trim();
    }

    public Integer getIsbindfinger() {
        return isbindfinger;
    }

    public void setIsbindfinger(Integer isbindfinger) {
        this.isbindfinger = isbindfinger;
    }

    public Integer getIsbindface() {
        return isbindface;
    }

    public void setIsbindface(Integer isbindface) {
        this.isbindface = isbindface;
    }

    public String getMemberidentifier() {
        return memberidentifier;
    }

    public void setMemberidentifier(String memberidentifier) {
        this.memberidentifier = memberidentifier == null ? null : memberidentifier.trim();
    }

    public String getGroupidentifier() {
        return groupidentifier;
    }

    public void setGroupidentifier(String groupidentifier) {
        this.groupidentifier = groupidentifier == null ? null : groupidentifier.trim();
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname == null ? null : groupname.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}