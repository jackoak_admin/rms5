package com.qlhx.dao;

import java.util.List;

import com.qlhx.model.BaseAccessRecord;
import com.qlhx.model.BaseVisitor;
import org.apache.ibatis.annotations.Param;

public interface BaseVisitorMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BaseVisitor record);

    int insertSelective(BaseVisitor record);

    BaseVisitor selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BaseVisitor record);

    int updateByPrimaryKey(BaseVisitor record);
    
    List<BaseVisitor> findAll();
    
    BaseVisitor selectByIdNum(String idNum);
    /**
     * <p>
     * Title:根据身份证号码查询访客信息
     * </p>
     * <p>
     * Description:根据身份证号码查询访客信息
     * </p>
     *
     * @param idNum
     *            身份证号码
     * @return
     * @throws Exception
     */
    BaseVisitor findVisitorByIdNum(@Param("idNum") String idNum) throws Exception;
    BaseVisitor findVisitorByIdPhone(@Param("phone") String phone) throws Exception;
}