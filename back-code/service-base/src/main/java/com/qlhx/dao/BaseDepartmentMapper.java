package com.qlhx.dao;

import com.qlhx.model.BaseDepartment;

public interface BaseDepartmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BaseDepartment record);

    int insertSelective(BaseDepartment record);

    BaseDepartment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BaseDepartment record);

    int updateByPrimaryKey(BaseDepartment record);
}