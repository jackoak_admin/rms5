package com.qlhx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.qlhx.model.BaseAccessRecord;

public interface BaseAccessRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BaseAccessRecord record);

    int insertSelective(BaseAccessRecord record);

    BaseAccessRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BaseAccessRecord record);

    int updateByPrimaryKey(BaseAccessRecord record);
    
    Integer findAccessRecordPageCount(BaseAccessRecord recode) throws Exception;
    
    List<BaseAccessRecord> findAccessRecord(BaseAccessRecord recode) throws Exception;
    
    BaseAccessRecord findAccessRecordByCardNum(@Param("cardNum") String cardNum) ;
}