package com.qlhx.dao;

import org.apache.ibatis.annotations.Param;

import com.qlhx.model.User;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    User selectByUserNameAndPassword(@Param("username") String userName ,@Param("password") String password);
}