package com.qlhx.dao;

import java.util.List;

import com.qlhx.model.BaseVisitReasons;

public interface BaseVisitReasonsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BaseVisitReasons record);

    int insertSelective(BaseVisitReasons record);

    BaseVisitReasons selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BaseVisitReasons record);

    int updateByPrimaryKey(BaseVisitReasons record);
    
    List<BaseVisitReasons> list();
}