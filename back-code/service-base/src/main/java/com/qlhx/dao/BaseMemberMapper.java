package com.qlhx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.qlhx.model.BaseMember;

public interface BaseMemberMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BaseMember record);

    int insertSelective(BaseMember record);

    BaseMember selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseMember record);

    int updateByPrimaryKey(BaseMember record);
    
    BaseMember selectByCardNum(String cardNum);
    
    List<BaseMember> findUserByPhoneOrName(@Param("param") String param,
    	    @Param("pageindex") Integer pageindex,
    	    @Param("pagesize") Integer pagesize) throws Exception;

        Integer findUserCountByPhoneOrName(@Param("param") String param)
    	    throws Exception;
}