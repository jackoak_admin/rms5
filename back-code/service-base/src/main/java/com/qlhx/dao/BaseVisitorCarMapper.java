package com.qlhx.dao;

import com.qlhx.model.BaseVisitorCar;
import org.apache.ibatis.annotations.Param;

public interface BaseVisitorCarMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BaseVisitorCar record);

    int insertSelective(BaseVisitorCar record);

    BaseVisitorCar selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BaseVisitorCar record);

    int updateByPrimaryKey(BaseVisitorCar record);

    /**
     *
     * <p>
     * Title:根据访客记录id查询访客车辆信息
     * </p>
     * <p>
     * Description:
     * </p>
     *
     * @param arId
     * @return
     * @throws Exception
     */
    BaseVisitorCar findCarByArId(@Param("arId") Integer arId) throws Exception;
}