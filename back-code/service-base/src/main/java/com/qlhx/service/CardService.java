package com.qlhx.service;

import com.qlhx.model.Card;

public interface CardService {
	int insertSelective(Card record);
	
	Card selectByCardNum(String cardNum);

}
