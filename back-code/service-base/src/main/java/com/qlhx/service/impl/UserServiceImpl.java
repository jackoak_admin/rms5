package com.qlhx.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.UserMapper;
import com.qlhx.model.User;
import com.qlhx.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserMapper userMapper;

	@Override
	public User selectByUserNameAndPassword(String userName, String password) {
		return userMapper.selectByUserNameAndPassword(userName, password);
	}

}
