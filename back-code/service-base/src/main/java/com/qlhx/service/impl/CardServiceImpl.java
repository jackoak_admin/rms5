package com.qlhx.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.CardMapper;
import com.qlhx.model.Card;
import com.qlhx.service.CardService;

@Service
public class CardServiceImpl implements CardService {
	
	@Autowired
	CardMapper cardMapper;

	@Override
	public int insertSelective(Card record) {
		return cardMapper.insertSelective(record);
	}

	@Override
	public Card selectByCardNum(String cardNum) {
		return cardMapper.selectByCardNum(cardNum);
	}
	
	

}
