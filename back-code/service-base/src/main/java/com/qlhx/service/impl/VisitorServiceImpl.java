package com.qlhx.service.impl;

import java.util.List;
import java.util.Map;

import com.qlhx.dao.BaseVisitorMapper;
import com.qlhx.model.BaseVisitor;
import com.qlhx.mybatis.BaseMybatisDao;
import com.qlhx.mybatis.page.Pagination;
import com.qlhx.service.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VisitorServiceImpl extends BaseMybatisDao<BaseVisitorMapper> implements VisitorService {

	@Autowired
	BaseVisitorMapper baseVisitorMapper;
	
	@Override
	public List<BaseVisitor> findAll() {
		return baseVisitorMapper.findAll();
	}

	@Override
	public Pagination<BaseVisitor> selectVisitorRecord(Map<String, Object> modelMap, Integer pageNo, Integer pageSize) {
		return super.findPage(modelMap, pageNo, pageSize);
	}

	@Override
	public BaseVisitor selectByPrimaryKey(Integer id) {
		return baseVisitorMapper.selectByPrimaryKey(id);
	}

	@Override
	public BaseVisitor findVisitorByIdNum(String idNum) throws Exception {
		return baseVisitorMapper.findVisitorByIdNum(idNum);
	}

	@Override
	public BaseVisitor findVisitorByIdPhone(String phone) throws Exception {
		return baseVisitorMapper.findVisitorByIdPhone(phone);
	}

	@Override
	public int updateByPrimaryKey(BaseVisitor record) {
		return baseVisitorMapper.updateByPrimaryKey(record);
	}

	@Override
	public int insertSelective(BaseVisitor record) {
		return baseVisitorMapper.insertSelective(record);
	}

}
