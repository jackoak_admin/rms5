package com.qlhx.service;

import java.util.List;
import java.util.Map;

import com.qlhx.model.BaseMember;
import com.qlhx.mybatis.page.Pagination;

public interface MemberService {
	
	Pagination<BaseMember> selectMemberRecord(Map<String, Object> modelMap, Integer pageNo, Integer pageSize);
	
	int insertSelective(BaseMember record);
	
	BaseMember selectByCardNum(String cardNum);
	
	List<BaseMember> findUserByPhoneOrName(String param, Integer pageindex,
            Integer pagesize) throws Exception;
	
	public Integer findUserCountByPhoneOrName(String param) throws Exception;
}
