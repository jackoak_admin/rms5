package com.qlhx.service;

import java.util.List;
import java.util.Map;

import com.qlhx.model.BaseVisitor;
import com.qlhx.mybatis.page.Pagination;
import org.apache.ibatis.annotations.Param;

public interface VisitorService {
	
	Pagination<BaseVisitor> selectVisitorRecord(
		    Map<String, Object> modelMap, Integer pageNo, Integer pageSize);
	
	List<BaseVisitor> findAll();
	
	BaseVisitor selectByPrimaryKey(Integer id);

	BaseVisitor findVisitorByIdNum(@Param("idNum") String idNum) throws Exception;
	BaseVisitor findVisitorByIdPhone(@Param("phone") String phone) throws Exception;
	int updateByPrimaryKey(BaseVisitor record);
	int insertSelective(BaseVisitor record);

}
