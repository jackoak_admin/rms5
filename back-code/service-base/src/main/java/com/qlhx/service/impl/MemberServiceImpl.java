package com.qlhx.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.BaseMemberMapper;
import com.qlhx.model.BaseMember;
import com.qlhx.mybatis.BaseMybatisDao;
import com.qlhx.mybatis.page.Pagination;
import com.qlhx.service.MemberService;

@Service
public class MemberServiceImpl extends BaseMybatisDao<BaseMemberMapper> implements MemberService {

	@Autowired
    BaseMemberMapper baseMemberMapper;
	
	@Override
	public Pagination<BaseMember> selectMemberRecord(Map<String, Object> modelMap, Integer pageNo, Integer pageSize) {
		return super.findPage(modelMap, pageNo, pageSize);
	}

	@Override
	public int insertSelective(BaseMember record) {
		return baseMemberMapper.insertSelective(record);
	}

	@Override
	public BaseMember selectByCardNum(String cardNum) {
		return baseMemberMapper.selectByCardNum(cardNum);
	}

	@Override
	public List<BaseMember> findUserByPhoneOrName(String param, Integer pageindex, Integer pagesize) throws Exception {
		return baseMemberMapper.findUserByPhoneOrName(param, pageindex, pagesize);
	}

	@Override
	public Integer findUserCountByPhoneOrName(String param) throws Exception {
		return baseMemberMapper.findUserCountByPhoneOrName(param);
	}

	
	
}
