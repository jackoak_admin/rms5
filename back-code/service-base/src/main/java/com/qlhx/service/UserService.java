package com.qlhx.service;

import com.qlhx.model.User;

public interface UserService {
	

    User selectByUserNameAndPassword(String userName ,String password);


}
