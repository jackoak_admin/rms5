package com.qlhx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.BaseVisitReasonsMapper;
import com.qlhx.model.BaseVisitReasons;
import com.qlhx.service.VisitorReasonsService;

@Service
public class VisitorReasonsServiceImpl implements VisitorReasonsService {
	
	@Autowired
	BaseVisitReasonsMapper baseVisitReasonsMapper;

	@Override
	public List<BaseVisitReasons> list() {
		return baseVisitReasonsMapper.list();
	}
	
	

}
