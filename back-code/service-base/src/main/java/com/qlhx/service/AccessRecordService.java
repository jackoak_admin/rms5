package com.qlhx.service;

import java.util.List;
import java.util.Map;

import com.qlhx.common.model.Result;
import com.qlhx.model.BaseAccessRecord;
import com.qlhx.mybatis.page.Pagination;

public interface AccessRecordService {
	
	Pagination<BaseAccessRecord> selectAccessRecord(Map<String, Object> modelMap, Integer pageNo, Integer pageSize);
	

	Result<Map<String, Object>> SaveAccessRecord(BaseAccessRecord record) throws Exception;
	
	Result<Map<String, Object>> addEntourage(BaseAccessRecord record) throws Exception;
	
    Integer findAccessRecordPageCount(BaseAccessRecord record) throws Exception;
    
    List<BaseAccessRecord> findAccessRecord(BaseAccessRecord record) throws Exception;
    
    BaseAccessRecord findAccessRecordByCardNum(String cardNum);

	int updateByPrimaryKeySelective(BaseAccessRecord record);

	int insertSelective(BaseAccessRecord record);
}
