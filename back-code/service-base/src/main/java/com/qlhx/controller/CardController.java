package com.qlhx.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.common.model.ApiResult;
import com.qlhx.model.Card;
import com.qlhx.model.LoginModel;
import com.qlhx.model.User;
import com.qlhx.service.CardService;
import com.qlhx.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "卡片接口", description = "卡片接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("card")
public class CardController {
	
	private  final Logger logger = LoggerFactory.getLogger(CardController.class);
	
	@Autowired
	CardService cardService;
	

	
	@ApiOperation(value ="发卡" ,notes ="发卡")
	@RequestMapping(value = "/save",method = RequestMethod.POST, produces = {
    "application/json"}, consumes = {"application/json"})
	public @ResponseBody ApiResult<String> save(@RequestBody Card card, HttpServletRequest request
			) 
	{
		ApiResult<String> result = new ApiResult<String>();
		try {
			cardService.insertSelective(card);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("用户登录发生错误", e);
			result.getErrorResult(e);
		}

		return result;

	}
	
	@ApiOperation(value ="查询卡是否存在" ,notes ="查询卡是否存在")
	@RequestMapping(value = "/power/{cardNum}",method = RequestMethod.GET, produces = {
    "application/json"})
	public @ResponseBody ApiResult<String> power(@PathVariable String cardNum)
	{
		ApiResult<String> result = new ApiResult<String>();
		Card card = cardService.selectByCardNum(cardNum);
		if(card == null)
		{
			result.setCode(ApiResult.NO_FOUND_DATA);
			result.setMsg("没有权限");
		}
		logger.info("======收到卡号:"+cardNum+" 验证结果:"+result.getMsg());
		return result;
	}
	
	

}
