package com.qlhx.modle.bo;

/**
 * Create by byl
 *
 * @versio 1.0.0
 * @Author baiyanlong
 * @date 2019/7/4 18:03
 * @description desc:
 */

import com.qlhx.modle.WechatPreapplygoupDetail;
import com.qlhx.modle.WechatWxuser;

/**
 * Created by yxn on 2018-05-19.
 */
public class WechatPreapplygoupDetailBo extends WechatPreapplygoupDetail {


    private WechatWxuser wechatWxuser;
    public WechatWxuser getWxuser() {
        return wechatWxuser;
    }

    public void setWxuser(WechatWxuser wechatWxuser) {
        this.wechatWxuser = wechatWxuser;
    }

}
