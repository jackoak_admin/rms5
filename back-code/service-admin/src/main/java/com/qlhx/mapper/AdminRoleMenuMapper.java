package com.qlhx.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.qlhx.model.AdminRoleMenu;

public interface AdminRoleMenuMapper {

	@Select("select * from admin_role_menu")
	List<AdminRoleMenu> selectByEmample(Object object);

}
