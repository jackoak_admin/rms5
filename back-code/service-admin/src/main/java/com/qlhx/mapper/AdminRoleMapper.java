package com.qlhx.mapper;

import com.qlhx.model.AdminRole;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AdminRoleMapper {


    @Select("select * from admin_role")
    List<AdminRole> selectByEmample(Object o);
}
