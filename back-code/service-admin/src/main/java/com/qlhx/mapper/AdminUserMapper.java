package com.qlhx.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.qlhx.model.AdminUser;

public interface AdminUserMapper {


	@Select("select * from admin_user")
	List<AdminUser> selectByEmample(Object object);

}
