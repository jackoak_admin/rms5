package com.qlhx.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.qlhx.model.AdminUser;
import com.qlhx.model.AdminUserRole;

@Transactional
public interface AdminUserService {

	List<AdminUser> findAll();


}
