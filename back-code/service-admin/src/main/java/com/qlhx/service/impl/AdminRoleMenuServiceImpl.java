package com.qlhx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.mapper.AdminRoleMenuMapper;
import com.qlhx.model.AdminRoleMenu;
import com.qlhx.service.AdminRoleMenuService;
@Service
public class AdminRoleMenuServiceImpl implements AdminRoleMenuService{

	@Autowired
	private AdminRoleMenuMapper adminrolemenumapper; 
	
	@Override
	public List<AdminRoleMenu> findAll() {
		// TODO Auto-generated method stub
		return this.adminrolemenumapper.selectByEmample(null);
	}

}
