package com.qlhx.service;

import java.util.List;

import com.qlhx.model.AdminRoleMenu;

public interface AdminRoleMenuService {

	List<AdminRoleMenu> findAll();

}
