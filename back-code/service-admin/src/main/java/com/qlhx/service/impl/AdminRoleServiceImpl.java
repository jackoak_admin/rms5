package com.qlhx.service.impl;

import com.qlhx.mapper.AdminRoleMapper;
import com.qlhx.model.AdminRole;
import com.qlhx.service.AdminRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminRoleServiceImpl  implements AdminRoleService {


    @Autowired
    private AdminRoleMapper adminRoleMapper;
    @Override
    public List<AdminRole> findAll() {
        return this.adminRoleMapper.selectByEmample(null);
    }
}
