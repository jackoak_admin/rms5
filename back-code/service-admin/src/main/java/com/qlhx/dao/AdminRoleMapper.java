package com.qlhx.dao;

import com.qlhx.model.AdminRole;

public interface AdminRoleMapper {


    boolean insertAdminRole(AdminRole obj);

    boolean deleteList(String [] ids);

}
