package com.qlhx.model;

import java.util.Date;

public class AdminUser {
    private int id;

    private String email;

    private String pswd;

    private Date create_time;

    private Date last_login_time;

    private int status;

    private String member_identifer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPswd() {
        return pswd;
    }

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(Date last_login_time) {
        this.last_login_time = last_login_time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMember_identifer() {
        return member_identifer;
    }

    public void setMember_identifer(String member_identifer) {
        this.member_identifer = member_identifer;
    }
}
