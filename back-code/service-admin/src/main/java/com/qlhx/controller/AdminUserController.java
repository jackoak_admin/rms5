package com.qlhx.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.model.AdminRole;
import com.qlhx.model.AdminUser;
import com.qlhx.service.AdminUserService;

@CrossOrigin(maxAge=3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("/admin")
public class AdminUserController {
	
	@Autowired
	private AdminUserService adminuserservice;
	
	@RequestMapping(value = "/list" ,method = RequestMethod.GET)
	@ResponseBody
    public List<AdminUser> findAll(){
        return this.adminuserservice.findAll();
    }
	
}
