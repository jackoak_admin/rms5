package com.qlhx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.model.AdminRoleMenu;
import com.qlhx.service.AdminRoleMenuService;

@CrossOrigin(maxAge=3600)
@RestController
@Scope(value="prototype")
@RequestMapping("/menu")
public class AdminRoleMenuController {

	@Autowired
	private AdminRoleMenuService adminrolemenuservice;
	
	 @RequestMapping(value = "/list" ,method = RequestMethod.GET)
	 @ResponseBody
	 public List<AdminRoleMenu> findAll(){
		 return this.adminrolemenuservice.findAll();
	    }
}
