package com.qlhx.controller;


import com.qlhx.model.AdminRole;
import com.qlhx.service.AdminRoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype")
@RequestMapping("/user")
public class AdminRoleController {

    @Autowired
    private AdminRoleService adminroleservice;

    @RequestMapping(value = "/list" ,method = RequestMethod.GET)
    @ResponseBody
    public List<AdminRole> findAll(){
        return this.adminroleservice.findAll();
    }
    
    
}
