package com.qlhx.service;

import java.util.List;

import com.qlhx.model.Rule;

public interface RuleService {
	
    List<Rule> list();
	
	int insertSelective(Rule record);

}
