package com.qlhx.service;

import java.util.List;

import com.qlhx.model.Path;

public interface PathService {
	
    List<Path> list();
	
	int insertSelective(Path record);

	Path selectByPrimaryKey(Integer id);
}
