package com.qlhx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.PathMapper;
import com.qlhx.model.Path;
import com.qlhx.service.PathService;

@Service
public class PathServiceImpl implements PathService {

	@Autowired
	PathMapper pathMapper;
	
	@Override
	public List<Path> list() {
		return pathMapper.list();
	}

	@Override
	public int insertSelective(Path record) {
		return pathMapper.insertSelective(record);
	}

	@Override
	public Path selectByPrimaryKey(Integer id) {
		return pathMapper.selectByPrimaryKey(id);
	}
	
	

}
