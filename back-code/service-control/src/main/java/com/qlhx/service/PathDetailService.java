package com.qlhx.service;

import java.util.List;

import com.qlhx.model.PathDetail;

public interface PathDetailService {
	
    List<PathDetail> list(Integer pathId);
	
	int insertSelective(PathDetail record);
	
	int deleteByPrimaryKey(Integer id);

}
