package com.qlhx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.KeyMapper;
import com.qlhx.model.Key;
import com.qlhx.service.KeyService;

@Service
public class KeyServiceImpl implements KeyService {

	@Autowired
	KeyMapper keyMapper;
	
	@Override
	public List<Key> list() {
		return keyMapper.list();
	}

	@Override
	public int insertSelective(Key record) {
		return keyMapper.insertSelective(record);
	}

	@Override
	public Key selectByPrimaryKey(Integer id) {
		return keyMapper.selectByPrimaryKey(id);
	}
	
	

}
