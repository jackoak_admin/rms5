package com.qlhx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.RuleMapper;
import com.qlhx.model.Rule;
import com.qlhx.service.RuleService;

@Service
public class RuleServiceImpl implements RuleService {

	@Autowired
	RuleMapper ruleMapper;
	
	@Override
	public List<Rule> list() {
		return ruleMapper.list();
	}

	@Override
	public int insertSelective(Rule record) {
		return ruleMapper.insertSelective(record);
	}

}
