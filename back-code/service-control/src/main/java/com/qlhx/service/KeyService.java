package com.qlhx.service;

import java.util.List;

import com.qlhx.model.Key;

public interface KeyService {
	
    List<Key> list();
	
	int insertSelective(Key record);
	
	Key selectByPrimaryKey(Integer id);

}
