/**
 * 
 */
package com.qlhx.service;

import java.util.List;

import com.qlhx.model.PersonGroup;
import com.qlhx.model.PersonGroupImpl;

/**
 * @author YF20150805
 *
 */
public interface PersonGroupService {
	
	List<PersonGroupImpl> list();
	
	int insertSelective(PersonGroup record);

	PersonGroupImpl selectByIdentifier(String identifier);
}
