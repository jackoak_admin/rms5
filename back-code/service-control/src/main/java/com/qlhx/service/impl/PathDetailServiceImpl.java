package com.qlhx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.PathDetailMapper;
import com.qlhx.model.PathDetail;
import com.qlhx.service.PathDetailService;

@Service
public class PathDetailServiceImpl implements PathDetailService {

	@Autowired
	PathDetailMapper pathDetailMapper;
	
	@Override
	public List<PathDetail> list(Integer pathId) {
		return pathDetailMapper.list(pathId);
	}

	@Override
	public int insertSelective(PathDetail record) {
		return pathDetailMapper.insertSelective(record);
	}

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return pathDetailMapper.deleteByPrimaryKey(id);
	}

}
