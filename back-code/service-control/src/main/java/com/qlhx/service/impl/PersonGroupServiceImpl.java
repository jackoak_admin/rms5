package com.qlhx.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.PersonGroupMapper;
import com.qlhx.model.PersonGroup;
import com.qlhx.model.PersonGroupImpl;
import com.qlhx.service.PersonGroupService;

@Service
public class PersonGroupServiceImpl implements PersonGroupService {

	@Autowired
	PersonGroupMapper personGroupMapper;
	
	@Override
	public List<PersonGroupImpl> list() {
		return personGroupMapper.list();
	}

	@Override
	public int insertSelective(PersonGroup record) {
		record.setCreatedate(new Date());
		return personGroupMapper.insertSelective(record);
	}

	@Override
	public PersonGroupImpl selectByIdentifier(String identifier) {
		return personGroupMapper.selectByIdentifier(identifier);
	}

}
