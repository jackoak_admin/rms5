package com.qlhx.dao;

import java.util.List;

import com.qlhx.model.PersonGroup;
import com.qlhx.model.PersonGroupImpl;

public interface PersonGroupMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PersonGroup record);

    int insertSelective(PersonGroup record);

    PersonGroup selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PersonGroup record);

    int updateByPrimaryKey(PersonGroup record);
    
    List<PersonGroupImpl> list();
    
    PersonGroupImpl selectByIdentifier(String identifier);
}