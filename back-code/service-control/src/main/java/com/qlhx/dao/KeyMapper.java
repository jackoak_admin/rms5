package com.qlhx.dao;

import java.util.List;

import com.qlhx.model.Key;

public interface KeyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Key record);

    int insertSelective(Key record);

    Key selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Key record);

    int updateByPrimaryKey(Key record);
    
    List<Key> list();
}