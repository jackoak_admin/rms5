package com.qlhx.dao;

import java.util.List;

import com.qlhx.model.PathDetail;

public interface PathDetailMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PathDetail record);

    int insertSelective(PathDetail record);

    PathDetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PathDetail record);

    int updateByPrimaryKey(PathDetail record);
    
    List<PathDetail> list(Integer pathId);
}