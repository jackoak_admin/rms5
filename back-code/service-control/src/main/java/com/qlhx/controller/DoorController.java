package com.qlhx.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.qlhx.common.model.ApiResult;
import com.qlhx.common.model.DataGridResult;
import com.qlhx.common.util.DateUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "门禁接口", description = "门禁接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("contr")
public class DoorController {
	
	private  final Logger logger = LoggerFactory.getLogger(DoorController.class);
	
	@Autowired
	RedisTemplate<String, Object> myRedis;
	
	@Autowired
	RestTemplate restTemplate;
	
	private final String LOG_KEY ="logKey";
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "list", method = RequestMethod.POST ,produces = { "application/json" })
	public @ResponseBody DataGridResult list(Integer offset, Integer limit) {

		HttpHeaders headers = new HttpHeaders();
		MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
		headers.setContentType(type);
		HttpEntity formEntity = new HttpEntity(null, headers);
		ResponseEntity<ApiResult> res = restTemplate.postForEntity("http://service-door/entranceGuard/list", formEntity,
				ApiResult.class);
		ApiResult<List> apiResult = res.getBody();
		DataGridResult dataGridResult = new DataGridResult();
		dataGridResult.setTotal(apiResult.getContent().size());
		dataGridResult.setRows(apiResult.getContent());
		return dataGridResult;
	}
	
	@ApiOperation(value ="获得日志" ,notes ="获得日志")
	@RequestMapping(value = "/getlog",method = RequestMethod.POST, produces = {
    "application/json"}, consumes = {"application/json"})
	public @ResponseBody ApiResult<String> getlog(HttpServletRequest request
			) 
	{
		ApiResult<String> result = new ApiResult<String>();
		String sumValue = "";
		while(true)
		{
			String value = (String)myRedis.opsForList().rightPop(LOG_KEY);
			if(value != null)
			{
				sumValue += "["+DateUtil.dateToStringWithTime()+"] " + value + "||";
			}
			else
			{
				break;
			}
					
		}
		result.setContent(sumValue);
		return result;

	}
	
	@ApiOperation(value ="存入日志" ,notes ="存入日志")
	@RequestMapping(value = "/putlog",method = RequestMethod.POST)
	public @ResponseBody ApiResult<String> putlog(@RequestBody String info, HttpServletRequest request
			) 
	{
		logger.info("====收到信息:"+info);
		ApiResult<String> result = new ApiResult<String>();
		myRedis.opsForList().leftPush(LOG_KEY,info);
		
		if(myRedis.opsForList().size(LOG_KEY)> 10)
		{
			myRedis.opsForList().trim(LOG_KEY, 0, 10);
		}
		return result;

	}
	
	@ApiOperation(value ="远程开门" ,notes ="远程开门")
	@RequestMapping(value = "/openDoor/{sn}",method = RequestMethod.GET, produces = {
    "application/json"})
	public @ResponseBody ApiResult<String> openDoor(@PathVariable long sn)
	{
		ApiResult result = null;
		OpenDoorModel openDoorModel = new OpenDoorModel();
		openDoorModel.setSn(sn);
		openDoorModel.setDoorNo(1);
		try {
			 HttpHeaders headers = new HttpHeaders();
		        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
		        headers.setContentType(type);
			HttpEntity<OpenDoorModel> formEntity = new HttpEntity<OpenDoorModel>(openDoorModel, headers);
			result = restTemplate.postForObject("http://service-door/entranceGuard/openDoor", formEntity, ApiResult.class);
//			RestTemplateUitl<OpenDoorModel,ApiResult<String>> restTemplateUitl =new RestTemplateUitl<OpenDoorModel,ApiResult<String>>();
//			result = restTemplateUitl.postByJson(restTemplate, "http://service-door/entranceGuard/openDoor", openDoorModel,ApiResult.class);
		} catch (Exception e) {
			if(result == null)
			{
				result = new ApiResult<String>();
			}
			result.getErrorResult(e);
		}
		
		
		return result;
	}
	
	

}
