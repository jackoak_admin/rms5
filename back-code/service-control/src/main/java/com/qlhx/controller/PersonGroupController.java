package com.qlhx.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.common.model.ApiResult;
import com.qlhx.model.PersonGroup;
import com.qlhx.model.PersonGroupImpl;
import com.qlhx.service.PersonGroupService;

import io.swagger.annotations.Api;

@Api(value = "人员分组接口", description = "人员分组接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("personGroup")
public class PersonGroupController {
	
	private  final Logger logger = LoggerFactory.getLogger(PersonGroupController.class);
	
	
	@Autowired
	PersonGroupService personGroupService;
	
	@RequestMapping(value = "list", method = RequestMethod.POST ,produces = { "application/json" })
	public @ResponseBody List<PersonGroupImpl> list() {

		List<PersonGroupImpl> list = personGroupService.list();
		return list;
	}
	
	
	@RequestMapping(value = "addOrUpdate", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody ApiResult<String> addOrUpdate(PersonGroup personGroup, HttpServletRequest request) {
		ApiResult<String> result = new ApiResult<String>();
		personGroup.setGroupIdentifier(UUID.randomUUID().toString());
		personGroupService.insertSelective(personGroup);
		return result;
	}

}
