package com.qlhx.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.common.model.ApiResult;
import com.qlhx.model.Key;
import com.qlhx.model.Rule;
import com.qlhx.service.RuleService;

import io.swagger.annotations.Api;

@Api(value = "规则接口", description = "规则接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("rule")
public class RuleController {
	
	private  final Logger logger = LoggerFactory.getLogger(RuleController.class);
	
	
	@Autowired
	RuleService ruleService;
	
	@RequestMapping(value = "list", method = RequestMethod.POST ,produces = { "application/json" })
	public @ResponseBody List<Rule> list() {

		List<Rule> list = ruleService.list();
		return list;
	}
	
	
	@RequestMapping(value = "addOrUpdate", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody ApiResult<String> addOrUpdate(Rule rule, HttpServletRequest request) {
		ApiResult<String> result = new ApiResult<String>();
		String ruleValue = "00000000000000000000000000000000";
		String[] ruleArr = rule.getRule().split(",");
		for(String index : ruleArr)
		{
			ruleValue = replaceString(ruleValue,"1",Integer.parseInt(index));
		}
		rule.setRule(ruleValue);
		rule.setCreatedate(new Date());
		ruleService.insertSelective(rule);
		return result;
	}
	
//	public static void main(String[] args) {
//		String biometricsValue = "0000000000000000";
//		biometricsValue =  replaceString(biometricsValue,"1",1);
//		biometricsValue =  replaceString(biometricsValue,"1",5);
//        System.out.println(biometricsValue);
//    }
	
    public static String replaceString(String str, String rstr, int a) {
        return str.substring(0, a - 1) + rstr + str.substring(a);
    }

}
