package com.qlhx.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.common.model.ApiResult;
import com.qlhx.util.Constant;

import io.swagger.annotations.Api;

@Api(value = "过滤器接口", description = "过滤器接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("filter")
public class FilterController {
	
	private  final Logger logger = LoggerFactory.getLogger(FilterController.class);
	
	@Autowired
	RedisTemplate<String, Object> myRedis;
	
	@RequestMapping(value = "setFilter", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody ApiResult<String> setFilter(String filter, HttpServletRequest request) {
		ApiResult<String> result = new ApiResult<String>();
		try {
			myRedis.opsForValue().set(Constant.FILTER_KEY,filter);
		} catch (Exception e) {
			result.getErrorResult(e);
		}
		return result;
	}
	
	@RequestMapping(value = "getFilter", method = RequestMethod.GET, produces = { "application/json" })
	public @ResponseBody ApiResult<String> getFilter(Integer filter, HttpServletRequest request) {
		ApiResult<String> result = new ApiResult<String>();
		try {
			String filterValue = (String)myRedis.opsForValue().get(Constant.FILTER_KEY);
			if(filterValue == null)
			{
				filterValue = "0";
			}
			result.setContent(filterValue);
		} catch (Exception e) {
			result.getErrorResult(e);
		}
		return result;
	}
	
	


}
