package com.qlhx.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.common.model.ApiResult;
import com.qlhx.common.util.DateUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "凭证接口", description = "凭证接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("logger")
public class LoggerController {

	@Autowired
	RedisTemplate<String, Object> myRedis;

	private final String LOG_KEY = "logKey";

	@ApiOperation(value = "获得日志", notes = "获得日志")
	@RequestMapping(value = "/getlog", method = RequestMethod.GET, produces = { "application/json" })
	public @ResponseBody ApiResult<String> getlog(HttpServletRequest request) {
		ApiResult<String> result = new ApiResult<String>();
		String sumValue = "";
		while (true) {
			String value = (String) myRedis.opsForList().rightPop(LOG_KEY);
			if (value != null) {
				sumValue += value + ";";
			} else {
				break;
			}

		}
		result.setContent(sumValue);
		return result;

	}
}
