package com.qlhx.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.common.model.ApiResult;
import com.qlhx.model.Path;
import com.qlhx.model.PathDetail;
import com.qlhx.model.Rule;
import com.qlhx.service.PathDetailService;
import com.qlhx.service.PathService;

import io.swagger.annotations.Api;

@Api(value = "路径接口", description = "路径接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("path")
public class PathController {
	
	private  final Logger logger = LoggerFactory.getLogger(PathController.class);
	
	
	@Autowired
	PathService pathService;
	
	@Autowired
	PathDetailService pathDetailService;
	
	@RequestMapping(value = "list", method = RequestMethod.POST ,produces = { "application/json" })
	public @ResponseBody List<Path> list() {

		List<Path> list = pathService.list();
		return list;
	}
	
	
	@RequestMapping(value = "addOrUpdate", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody ApiResult<String> addOrUpdate(Path path, HttpServletRequest request) {
		ApiResult<String> result = new ApiResult<String>();
		path.setCreatedate(new Date());
		pathService.insertSelective(path);
		return result;
	}
	
	@RequestMapping(value = "detailList", method = RequestMethod.POST ,produces = { "application/json" })
	public @ResponseBody List<PathDetail> detailList(Integer pathId) {

		List<PathDetail> list = pathDetailService.list(pathId);
		return list;
	}
	
	@RequestMapping(value = "detailAddOrUpdate", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody ApiResult<String> addOrUpdate(PathDetail pathDetail, HttpServletRequest request) {
		ApiResult<String> result = new ApiResult<String>();
		pathDetail.setCreatedate(new Date());
		pathDetailService.insertSelective(pathDetail);
		return result;
	}
	
	@RequestMapping(value = "delDetail", method = RequestMethod.GET ,produces = { "application/json" })
	public @ResponseBody ApiResult<String> delDetail(Integer id) {

		ApiResult<String> result = new ApiResult<String>();
		pathDetailService.deleteByPrimaryKey(id);
		return result;
	}

}
