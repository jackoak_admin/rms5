package com.qlhx.controller;

import io.swagger.annotations.ApiModelProperty;

public class OpenDoorModel {
	
	@ApiModelProperty(value = "sn")
	private Long sn;
	
	@ApiModelProperty(value = "门号，如1号门，2号门")
	private Integer doorNo;
	
	public Long getSn() {
		return sn;
	}

	public void setSn(Long sn) {
		this.sn = sn;
	}

	public Integer getDoorNo() {
		return doorNo;
	}

	public void setDoorNo(Integer doorNo) {
		this.doorNo = doorNo;
	}


}
