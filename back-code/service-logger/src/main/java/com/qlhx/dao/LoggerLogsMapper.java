package com.qlhx.dao;

import java.util.List;

import com.qlhx.model.LoggerLogs;

public interface LoggerLogsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LoggerLogs record);

    int insertSelective(LoggerLogs record);

    LoggerLogs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LoggerLogs record);

    int updateByPrimaryKey(LoggerLogs record);
    
    List<LoggerLogs> list();
}