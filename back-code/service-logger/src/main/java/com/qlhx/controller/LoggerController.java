package com.qlhx.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qlhx.model.LoggerLogs;
import com.qlhx.service.LoggerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Created by yxn on 2018-06-04.
 */

@Api(value = "日志接口", description = "日志接口")
@CrossOrigin(maxAge = 3600)
@Controller
@Scope(value = "prototype")
@RequestMapping("logger")
public class LoggerController {
	private  final Logger logger = LoggerFactory.getLogger(LoggerController.class);

    @Autowired
    private LoggerService loggerService;


	@ApiOperation(value = "日志列表", notes = "日志列表")
	@RequestMapping(value = "list", method = RequestMethod.POST, produces = { "application/json" })
	public @ResponseBody List<LoggerLogs> list(HttpServletRequest request) {
		List<LoggerLogs> list = loggerService.list();
		return list;
	}




}
