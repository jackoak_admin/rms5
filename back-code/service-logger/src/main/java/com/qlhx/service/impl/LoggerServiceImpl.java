package com.qlhx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.dao.LoggerLogsMapper;
import com.qlhx.model.LoggerLogs;
import com.qlhx.service.LoggerService;

@Service
public class LoggerServiceImpl implements LoggerService {
	
	@Autowired
	LoggerLogsMapper loggerLogsMapper;

	@Override
	public int insertSelective(LoggerLogs record) {
		return loggerLogsMapper.insertSelective(record);
	}

	@Override
	public List<LoggerLogs> list() {
		return loggerLogsMapper.list();
	}
	
	

}
