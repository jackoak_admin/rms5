package com.qlhx.service;

import java.util.List;

import com.qlhx.model.LoggerLogs;

public interface LoggerService {
	
	int insertSelective(LoggerLogs record);
	
	List<LoggerLogs> list();

}
