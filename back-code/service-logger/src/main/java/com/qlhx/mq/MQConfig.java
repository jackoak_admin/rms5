package com.qlhx.mq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.qlhx.common.model.MQLogger;



@Configuration
public class MQConfig {


    @Bean
    public Queue createQueue(){
        return new Queue(MQLogger.ROUTING_KEY,true);
    }
} 
