package com.qlhx.mapper;

import java.util.List;
import java.util.Map;

import com.qlhx.model.USwingCardRecord;

public interface USwingCardRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(USwingCardRecord record);

    int insertSelective(USwingCardRecord record);

    USwingCardRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(USwingCardRecord record);

    int updateByPrimaryKey(USwingCardRecord record);
    
    List<USwingCardRecord> list(Map param);
}