package com.qlhx.mapper;

import java.util.List;

import com.qlhx.model.UEntranceGuard;

public interface UEntranceGuardMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UEntranceGuard record);

    int insertSelective(UEntranceGuard record);

    UEntranceGuard selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UEntranceGuard record);

    int updateByPrimaryKey(UEntranceGuard record);
    
    List<UEntranceGuard> list();
    
    UEntranceGuard selectBySn(String sn);
}