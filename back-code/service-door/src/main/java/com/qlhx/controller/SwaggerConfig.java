package com.qlhx.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc
@ComponentScan(basePackages = { "com.qlhx.controller" })
public class SwaggerConfig {

    @Bean
    public Docket customDocket() {
	return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
		.select()
		// 选择那些路径和api会生成document
		.apis(RequestHandlerSelectors
				.basePackage("com.qlhx.controller")) // 对所有api进行监控
		.paths(PathSelectors.any()) // 对所有路径进行监控
		.build(); 
    }

    private ApiInfo apiInfo() {
	ApiInfo apiInfo = new ApiInfo("设备管理接口", "接口测试", "V1.0.0", "",
		"lidahai", "", "");
	return apiInfo;
    }

} 