package com.qlhx.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qlhx.mapper.USwingCardRecordMapper;
import com.qlhx.model.USwingCardRecord;
import com.qlhx.service.SwingCardRecordService;

@Service
public class SwingCardRecordServiceImpl implements SwingCardRecordService 
{
	@Autowired
	USwingCardRecordMapper swingCardRecordMapper;

	@Override
	public int insertSelective(USwingCardRecord record) {
		return swingCardRecordMapper.insertSelective(record);
	}

	@Override
	public List<USwingCardRecord> list(String sn) {
		Map param =new HashMap();
		param.put("sn", sn);
		return swingCardRecordMapper.list(param);
	}
	
	

}
