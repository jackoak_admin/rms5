package com.qlhx.service;

import java.util.List;

import com.qlhx.model.USwingCardRecord;

/**
 * Created by rongcan on 2017/7/11.
 */
public interface SwingCardRecordService {

    int insertSelective(USwingCardRecord record);
	
	List<USwingCardRecord> list(String sn);
}
