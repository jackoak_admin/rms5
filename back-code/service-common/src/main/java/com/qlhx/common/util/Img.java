package com.qlhx.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.codec.binary.Base64;

public class Img {

    /**
     * 
     * <p>
     * Title:图片路径转base64字符串
     * </p>
     * <p>
     * Description:
     * </p>
     *
     * @param imgFile
     *            图片路径
     * @return base64字符串
     */
    public static String img2base64(String imgFile) {
	InputStream inputStream = null;
	byte[] data = null;
	try {
	    inputStream = new FileInputStream(imgFile);
	    data = new byte[inputStream.available()];
	    inputStream.read(data);
	    inputStream.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	// 加密
	return new String(new Base64().encode(data));
    }

    /**
     * 
     * <p>
     * Title:图片流转base64字符串
     * </p>
     * <p>
     * Description:
     * </p>
     *
     * @param imgFile
     *            图片路径
     * @return base64字符串
     */
    public static String imgStream2base64(File file) {
	InputStream inputStream = null;
	byte[] data = null;
	try {
	    inputStream = new FileInputStream(file);
	    data = new byte[inputStream.available()];
	    inputStream.read(data);
	    inputStream.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	// 加密
	return new String(new Base64().encode(data));
    }

    /**
     * 
     * <p>
     * Title:base64串转img
     * </p>
     * <p>
     * Description:
     * </p>
     *
     * @param imgStr
     *            图片base64字符串
     * @param path
     *            图片保存路径
     * @return bo0lean true:转换保存成功 false:失败
     * @throws Exception
     */
    public static boolean base642img(String imgStr, String path)
	    throws Exception {
	if (imgStr == null) {
	    return false;
	}
	try {
	    File pp = new File(new File(path).getParent());
	    if (!pp.exists()) {
		pp.mkdirs();
	    }

	    // 解密
	    byte[] b = new Base64().decode(imgStr.getBytes());
	    // 处理数据
	    for (int i = 0; i < b.length; ++i) {
		if (b[i] < 0) {
		    b[i] += 256;
		}
	    }
	    OutputStream out = new FileOutputStream(path);
	    out.write(b);
	    out.flush();
	    out.close();
	    return true;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	}
    }

    /**
     * 
     * <p>
     * Title:将图片base64串转换成临时文件并返回
     * </p>
     * <p>
     * Description:
     * </p>
     *
     * @param imgStr
     *            图片base64字符串
     * @param path
     *            图片保存路径
     * @return bo0lean true:转换保存成功 false:失败
     * @throws Exception
     */
    public static File base642TempFile(String imgStr) throws Exception {
	if (imgStr == null) {
	    return null;
	}
	try {
	    File file = File.createTempFile(System.currentTimeMillis() + "",
		    imgStr.split(",")[0].split("/")[1]);

	    // 解密
	    byte[] b = new Base64().decode(imgStr.getBytes());
	    // 处理数据
	    for (int i = 0; i < b.length; ++i) {
		if (b[i] < 0) {
		    b[i] += 256;
		}
	    }
	    FileOutputStream fos = new FileOutputStream(file);
	    fos.write(b);
	    fos.flush();
	    fos.close();
	    return file;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	}
    }

    public static void main(String[] args) {
	// System.out.println("start:"
	// + DateTime.getTime("yyyy-MM-dd HH:mm:ss.SSS"));
	System.out.println("=======================");
	String bi = Img.img2base64("D:/ldh1.jpg");

	System.out.println("图片base64字符串：" + bi + "\n");
	//
	// try {
	// Img.base642img(bi, "D:/testimg/123，ew23.png");
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// System.out
	// .println("end:" + DateTime.getTime("yyyy-MM-dd HH:mm:ss.SSS"));

	// System.out.println((int) Math.ceil(162 / 10d));

    }

}
