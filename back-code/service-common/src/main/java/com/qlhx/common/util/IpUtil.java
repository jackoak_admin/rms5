package com.qlhx.common.util;

import javax.servlet.http.HttpServletRequest;

public class IpUtil {
	public static String getIpAddr(HttpServletRequest request) {  
		   String ipFromNginx = getHeader(request, "X-Real-IP");  
		   if (ipFromNginx == null || ipFromNginx.equals("") )
		   {
			   String ip = request.getHeader("x-forwarded-for");  
		       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		           ip = request.getHeader("Proxy-Client-IP");  
		       }  
		       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		           ip = request.getHeader("WL-Proxy-Client-IP");  
		       }  
		       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
		           ip = request.getRemoteAddr();  
		       }  
		       return ip;  
		   }
		   else
		   {
			   return  ipFromNginx;  
		   }
	      
	   }  
	private static String getHeader(HttpServletRequest request, String headName) {  
	    String value = request.getHeader(headName);  
	    return value != null && !value.equals("") && !"unknown".equalsIgnoreCase(value) ? value : "";  
	}  

}
