package com.qlhx.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qlhx.model.Device;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "门禁接口", description = "门禁接口")
@CrossOrigin(maxAge = 3600)
@RestController
@Scope(value = "prototype") 
@RequestMapping("entranceGuard")
public class DeviceController {
	
	private  final Logger logger = LoggerFactory.getLogger(DeviceController.class);
	
	@ApiOperation(value ="门禁信息更新" ,notes ="门禁信息更新")
	@RequestMapping(value = "/update",method = RequestMethod.POST, produces = {
    "application/json"}, consumes = {"application/json"})
	public @ResponseBody Device devic(@RequestBody Device device, HttpServletRequest request
			) 
	{
		logger.info("======="+device.getName());
		logger.error("++++++++++");
		return device;
		
	}

}
