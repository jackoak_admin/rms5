/*
Navicat MySQL Data Transfer

Source Server         : 127 3307
Source Server Version : 50711
Source Host           : 127.0.0.1:3307
Source Database       : aivms5

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2019-07-04 13:45:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '角色名称',
  `type` varchar(10) DEFAULT NULL COMMENT '角色类型',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES ('1', '系统管理员', '888888', null, null);
INSERT INTO `admin_role` VALUES ('3', '权限角色', '100003', null, null);
INSERT INTO `admin_role` VALUES ('4', '组织关系', '100002', null, null);
INSERT INTO `admin_role` VALUES ('5', '访客中心', 'FK2017', null, null);
INSERT INTO `admin_role` VALUES ('6', '访客管理', 'FK6666', null, null);
INSERT INTO `admin_role` VALUES ('7', '考勤管理', 'KQ8888', null, null);
INSERT INTO `admin_role` VALUES ('8', '终端管理', 'ZD2017', null, null);

-- ----------------------------
-- Table structure for `admin_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permission`;
CREATE TABLE `admin_role_permission` (
  `rid` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `pid` bigint(20) DEFAULT NULL COMMENT '权限ID',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_role_permission
-- ----------------------------
INSERT INTO `admin_role_permission` VALUES ('4', '10', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '11', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '12', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '57', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '58', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '65', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '66', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '67', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '68', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '69', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '70', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '71', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '72', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '73', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '74', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '75', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '76', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '77', null, null);
INSERT INTO `admin_role_permission` VALUES ('4', '78', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '4', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '6', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '7', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '13', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '14', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '15', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '16', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '17', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '18', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '19', null, null);
INSERT INTO `admin_role_permission` VALUES ('3', '20', null, null);
INSERT INTO `admin_role_permission` VALUES ('7', '85', null, null);
INSERT INTO `admin_role_permission` VALUES ('7', '86', null, null);
INSERT INTO `admin_role_permission` VALUES ('8', '96', null, null);
INSERT INTO `admin_role_permission` VALUES ('8', '97', null, null);
INSERT INTO `admin_role_permission` VALUES ('5', '21', null, null);
INSERT INTO `admin_role_permission` VALUES ('5', '22', null, null);
INSERT INTO `admin_role_permission` VALUES ('5', '23', null, null);
INSERT INTO `admin_role_permission` VALUES ('5', '24', null, null);
INSERT INTO `admin_role_permission` VALUES ('5', '25', null, null);
INSERT INTO `admin_role_permission` VALUES ('5', '95', null, null);
INSERT INTO `admin_role_permission` VALUES ('5', '98', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '4', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '6', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '7', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '8', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '9', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '10', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '11', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '12', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '13', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '14', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '15', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '16', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '17', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '18', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '19', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '20', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '21', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '22', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '23', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '24', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '25', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '26', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '27', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '28', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '29', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '31', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '32', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '33', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '34', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '35', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '36', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '37', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '38', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '39', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '40', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '41', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '42', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '43', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '44', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '45', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '50', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '51', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '52', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '53', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '54', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '55', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '56', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '57', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '58', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '59', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '60', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '61', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '62', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '63', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '65', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '66', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '67', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '68', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '69', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '70', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '71', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '72', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '73', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '74', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '75', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '76', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '77', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '78', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '79', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '80', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '81', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '82', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '83', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '84', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '85', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '86', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '87', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '88', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '89', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '90', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '92', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '94', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '95', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '96', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '97', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '98', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '99', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '100', null, null);
INSERT INTO `admin_role_permission` VALUES ('1', '101', null, null);

-- ----------------------------
-- Table structure for `admin_user`
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '邮箱|登录帐号',
  `pswd` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` bigint(1) DEFAULT '1' COMMENT '1:有效，0:禁止登录（禁止登录为不在职状态）',
  `member_identifier` varchar(255) DEFAULT NULL COMMENT '与员工关联的唯一标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19196 DEFAULT CHARSET=utf8mb4 COMMENT='管理员及员工表';

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('19139', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19140', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19141', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19142', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19143', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19144', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19145', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19146', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19147', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19148', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19149', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19150', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19151', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19152', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19153', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19154', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19155', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19156', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19157', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19158', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19159', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19160', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19161', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19162', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19163', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19164', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19165', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19166', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19167', null, null, '2018-11-28 17:36:04', null, '1', null);
INSERT INTO `admin_user` VALUES ('19168', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19169', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19170', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19171', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19172', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19173', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19174', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19175', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19176', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19177', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19178', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19179', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19180', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19181', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19182', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19183', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19184', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19185', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19186', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19187', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19188', null, null, '2018-11-28 17:36:03', null, '1', null);
INSERT INTO `admin_user` VALUES ('19195', null, null, null, null, '1', null);

-- ----------------------------
-- Table structure for `admin_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_role`;
CREATE TABLE `admin_user_role` (
  `uid` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `rid` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_user_role
-- ----------------------------
INSERT INTO `admin_user_role` VALUES ('15', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('13', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('13', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('17', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('17', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('31', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('31', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('31', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('31', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('32', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('32', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('32', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('32', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('21', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('33', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('33', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('33', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('33', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('20', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('20', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('20', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('23', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('23', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('23', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('23', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('233', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('233', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('233', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('233', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('235', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('235', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('235', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('235', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('26', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('19', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('19', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('19', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('19', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('1371', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('4368', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('4369', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('19040', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('19040', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('19040', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('19040', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('19040', '6', null, null);
INSERT INTO `admin_user_role` VALUES ('19042', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('1039', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1039', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('1039', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('1039', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('1039', '6', null, null);
INSERT INTO `admin_user_role` VALUES ('1039', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1040', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1040', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('1040', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('1040', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1043', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1043', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1053', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1053', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1051', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1051', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1050', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1050', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1049', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1049', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1048', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1048', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1047', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1047', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1045', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1045', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1044', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1044', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1038', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('1038', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('1038', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('1038', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('1038', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('1046', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('19069', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('19069', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('19069', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('19069', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('19069', '6', null, null);
INSERT INTO `admin_user_role` VALUES ('19069', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('19089', '1', null, null);
INSERT INTO `admin_user_role` VALUES ('19089', '3', null, null);
INSERT INTO `admin_user_role` VALUES ('19089', '4', null, null);
INSERT INTO `admin_user_role` VALUES ('19089', '5', null, null);
INSERT INTO `admin_user_role` VALUES ('19089', '6', null, null);
INSERT INTO `admin_user_role` VALUES ('19089', '7', null, null);
INSERT INTO `admin_user_role` VALUES ('19089', '8', null, null);

-- ----------------------------
-- Table structure for `base_access_record`
-- ----------------------------
DROP TABLE IF EXISTS `base_access_record`;
CREATE TABLE `base_access_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访客记录表ID',
  `parentID` int(11) DEFAULT NULL COMMENT '随行人员父级ID',
  `rCode` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT '生成二维码的编码（生成规则：MD5(id+creatTime)生成的串，从第一位开始取值，每隔3位取一次，共取8位）',
  `vID` int(11) DEFAULT NULL COMMENT '访客ID',
  `uID` int(11) DEFAULT '0' COMMENT '被访人ID（u_user）',
  `createTime` datetime DEFAULT NULL COMMENT '访客信息记录时间',
  `startTime` datetime DEFAULT NULL COMMENT '访问开始时间',
  `endTime` datetime DEFAULT NULL COMMENT '访客结束时间',
  `logOffTime` datetime DEFAULT NULL COMMENT '注销访问时间',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '记录当前状态（0：进行中 1：超时注销 2：正常结束 3：未登记）',
  `reasons` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '来访事由',
  `unit` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '来访单位',
  `num` int(11) DEFAULT NULL COMMENT '来访人数',
  `type` int(11) DEFAULT '0' COMMENT '访客记录类型（0：非预约 1：预约）',
  `isPrintVoucher` int(11) DEFAULT '0' COMMENT '是否打印访客凭条（0：未打印 1：已打印）',
  `isPullCard` int(11) DEFAULT '0' COMMENT '是否发放卡（0：未发放 1：已发放）',
  `cardType` int(11) DEFAULT NULL COMMENT '发放卡片类型（0：IC卡 1 身份证id卡号  2 二维码）',
  `CardNum` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '发放卡卡号（999开头的为虚拟卡号）',
  `sitePhoto` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '访客现场照片',
  `isUpload` int(1) unsigned zerofill DEFAULT '0' COMMENT '是否上传公安网（0：未上传 1：上传1次 2：上传两次）',
  `yyID` int(11) DEFAULT NULL COMMENT '微信预约ID',
  `companyCode` varchar(50) DEFAULT NULL COMMENT '公司编号',
  `terminalCode` varchar(30) DEFAULT NULL COMMENT '终端编号',
  `zhuxiaoTerminalCode` varchar(30) DEFAULT NULL COMMENT '注销的设备编号',
  `terminalManagerId` int(11) DEFAULT NULL COMMENT '登录终端的员工id',
  `zhuxiaoManagerId` int(11) DEFAULT NULL COMMENT '注销操作者id',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记',
  `syncId` varchar(50) DEFAULT NULL COMMENT '同步id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104027 DEFAULT CHARSET=utf8mb4 COMMENT='访问记录表';

-- ----------------------------
-- Records of base_access_record
-- ----------------------------
INSERT INTO `base_access_record` VALUES ('104019', null, null, '1', '1', null, '2019-06-20 18:45:44', '2019-06-20 18:45:47', '2019-06-20 18:45:51', '0', '开会', null, null, '0', '0', '0', null, '234234', null, '0', null, null, null, null, null, null, '2019-06-20 18:46:18', null, null);
INSERT INTO `base_access_record` VALUES ('104020', '0', null, '100022', '1', null, '2019-06-26 14:58:09', '2019-06-26 23:59:59', null, '0', '消费', '的地方风格', null, null, null, null, '2', '14438485', '', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_access_record` VALUES ('104021', '0', null, '100022', '1', null, '2019-06-26 15:26:43', '2019-06-26 23:59:59', null, '0', '上访', '规范此次', null, null, null, null, '2', '446797466', '', null, null, null, null, null, null, null, '2019-06-30 15:34:30', null, null);
INSERT INTO `base_access_record` VALUES ('104022', '0', null, '100022', '7', '2019-07-01 14:18:52', '2019-07-01 14:18:52', '2019-07-01 23:59:59', null, '0', '其他', 'sfft', null, null, null, null, '2', '16538487', '', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_access_record` VALUES ('104023', '0', null, '100022', '7', '2019-07-01 14:19:23', '2019-07-01 14:19:24', '2019-07-01 23:59:59', null, '0', '上访', 'ff', null, null, null, null, '2', '13238488', 'sitePhoto\\2019-07-01\\130626199810248179\\\\130626199810248179_1561961963258.png', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_access_record` VALUES ('104024', '0', null, '100022', '7', '2019-07-01 14:22:57', '2019-07-01 14:22:58', '2019-07-01 23:59:59', null, '0', '玩耍', 'ffggf', null, null, null, null, '2', '19438489', 'sitePhoto\\2019-07-01\\130626199810248179\\\\130626199810248179_1561962177098.png', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_access_record` VALUES ('104025', '0', null, '100022', '7', '2019-07-01 17:47:21', '2019-07-01 17:47:20', '2019-07-01 23:59:59', null, '0', '其他', 'ffgg', null, null, null, null, '2', '18938490', 'sitePhoto\\2019-07-01\\130626199810248179\\\\130626199810248179_1561974441591.png', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_access_record` VALUES ('104026', '0', null, '100022', '7', '2019-07-01 17:52:25', '2019-07-01 17:52:27', '2019-07-01 23:59:59', null, '0', '会议', 'xxxxxx', null, null, null, null, '2', '15238491', '', null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `base_blacklist`
-- ----------------------------
DROP TABLE IF EXISTS `base_blacklist`;
CREATE TABLE `base_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vId` int(11) NOT NULL COMMENT '访客ID',
  `reason` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '访客黑名单原因',
  `companyCode` varchar(12) DEFAULT NULL COMMENT '公司编码',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COMMENT='访客黑名单表';

-- ----------------------------
-- Records of base_blacklist
-- ----------------------------
INSERT INTO `base_blacklist` VALUES ('14', '13', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('15', '14', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('16', '15', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('17', '16', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('18', '17', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('19', '18', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('20', '19', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('21', '20', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('22', '21', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('24', '114', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_blacklist` VALUES ('25', '57', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('27', '115', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('28', '107', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('29', '34', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('30', '116', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('31', '120', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('32', '122', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('36', '533', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('37', '619', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('38', '617', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_blacklist` VALUES ('39', '620', null, null, '2017-12-08 11:14:00', '2017-12-08 11:14:00');
INSERT INTO `base_blacklist` VALUES ('41', '1197', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_blacklist` VALUES ('42', '1199', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_blacklist` VALUES ('43', '1200', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_blacklist` VALUES ('44', '1043', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_blacklist` VALUES ('47', '1111', null, null, '2017-12-18 10:02:00', '2017-12-18 10:02:00');
INSERT INTO `base_blacklist` VALUES ('48', '1271', null, null, '2017-12-22 13:50:00', '2017-12-22 13:50:00');
INSERT INTO `base_blacklist` VALUES ('49', '1278', null, null, '2017-12-22 15:08:00', '2017-12-22 15:08:00');
INSERT INTO `base_blacklist` VALUES ('50', '33', '大海测试黑名单', null, '2017-12-26 11:01:00', '2017-12-26 11:01:00');

-- ----------------------------
-- Table structure for `base_department`
-- ----------------------------
DROP TABLE IF EXISTS `base_department`;
CREATE TABLE `base_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `value` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '部门名称',
  `parentId` int(11) DEFAULT NULL COMMENT '父级部门ID',
  `syncId` int(11) DEFAULT NULL COMMENT '同步ID',
  `syncParentId` int(11) DEFAULT NULL COMMENT '同步父级id',
  `companyNum` varchar(12) CHARACTER SET utf8 DEFAULT NULL COMMENT '企业编号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `syncId` (`syncId`,`companyNum`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COMMENT='部门表';

-- ----------------------------
-- Records of base_department
-- ----------------------------
INSERT INTO `base_department` VALUES ('3', '产品部', '1', null, null, '1');
INSERT INTO `base_department` VALUES ('4', '销售中心', null, null, null, '2');
INSERT INTO `base_department` VALUES ('5', '销售1', '4', null, null, '3');
INSERT INTO `base_department` VALUES ('6', '销售1-1', '4', null, null, '4');
INSERT INTO `base_department` VALUES ('8', '行政中心', '1', null, null, '5');
INSERT INTO `base_department` VALUES ('13', '测试', null, null, null, '6');
INSERT INTO `base_department` VALUES ('14', '数据测试', null, null, null, '7');
INSERT INTO `base_department` VALUES ('15', '测试市场', null, null, null, '8');
INSERT INTO `base_department` VALUES ('16', '测试销售', null, null, null, '9');
INSERT INTO `base_department` VALUES ('17', '测试产品', null, null, null, '11');
INSERT INTO `base_department` VALUES ('18', '测试车间', null, null, null, '12');
INSERT INTO `base_department` VALUES ('19', '测试行政', null, null, null, '13');
INSERT INTO `base_department` VALUES ('20', '测试质检', null, null, null, '14');
INSERT INTO `base_department` VALUES ('21', '测试', null, '1', null, '12345678');
INSERT INTO `base_department` VALUES ('23', '同步测试部门', null, '22', null, '12345679');
INSERT INTO `base_department` VALUES ('24', 'string', '0', '0', '0', '15');
INSERT INTO `base_department` VALUES ('25', '测试', null, '1', '0', '0000001');
INSERT INTO `base_department` VALUES ('26', '测试98', null, '98', null, '12345678');
INSERT INTO `base_department` VALUES ('27', '测试99', null, '99', null, '12345678');
INSERT INTO `base_department` VALUES ('28', '运营', '8', null, null, null);
INSERT INTO `base_department` VALUES ('29', '大门', null, '91', '0', '0001');
INSERT INTO `base_department` VALUES ('30', '大门右侧区1单元', null, '93', '92', '0001');
INSERT INTO `base_department` VALUES ('31', '大门右侧区1单元45层', null, '94', '93', '0001');
INSERT INTO `base_department` VALUES ('32', '大门右侧区1单元45层56号', null, '95', '94', '0001 ');
INSERT INTO `base_department` VALUES ('33', '大门右侧区', null, '92', '91', '0001');
INSERT INTO `base_department` VALUES ('35', '研发部', null, '1', '0', '1000001');
INSERT INTO `base_department` VALUES ('36', '财务部', null, '2', '0', '0001');
INSERT INTO `base_department` VALUES ('37', '行政人事部', null, '3', '0', '0001');
INSERT INTO `base_department` VALUES ('38', '大海测试', '1', null, null, null);
INSERT INTO `base_department` VALUES ('39', '大海测试3', '1', null, null, null);
INSERT INTO `base_department` VALUES ('40', '大海测试4', null, null, null, null);

-- ----------------------------
-- Table structure for `base_member`
-- ----------------------------
DROP TABLE IF EXISTS `base_member`;
CREATE TABLE `base_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户昵称',
  `sex` int(1) DEFAULT NULL COMMENT '性别（0：女 1：男 2：其他）',
  `nation` int(2) DEFAULT NULL COMMENT '民族编码（详见民族编码对照表u_nation）',
  `birthday` date DEFAULT NULL COMMENT '出生年月',
  `address` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '家庭住址（身份证）',
  `idNum` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证号码',
  `photo` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证头像图片地址（身份证号为图片名称）',
  `issuing` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证签发机构',
  `validityDateStart` datetime DEFAULT NULL COMMENT '身份证签发时间',
  `validityDateEnd` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证过期时间',
  `phone` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机号',
  `depID` int(11) DEFAULT NULL COMMENT '部门ID',
  `telephone` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT '座机号码',
  `ecardNum` varchar(20) DEFAULT NULL COMMENT '员工门禁卡卡号',
  `ecardEndTime` datetime DEFAULT NULL COMMENT '员工卡有效截止时间',
  `pinYin` varchar(60) DEFAULT NULL COMMENT '名字首字母拼音',
  `code` varchar(255) DEFAULT NULL,
  `type` int(2) DEFAULT '0' COMMENT '0 正式员工 1 临时员工 ',
  `syncId` varchar(50) DEFAULT NULL,
  `isbindfinger` int(2) DEFAULT NULL COMMENT '0 否 1 是',
  `isbindface` int(2) DEFAULT NULL,
  `memberIdentifier` varchar(50) DEFAULT NULL,
  `groupIdentifier` varchar(50) DEFAULT NULL,
  `groupName` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='管理员及员工表';

-- ----------------------------
-- Records of base_member
-- ----------------------------
INSERT INTO `base_member` VALUES ('1', '张三', '1', '1', '2019-06-12', '北京海淀区', '123456', null, null, null, null, '111111', '123456', null, '23423', null, 'zhangSan', null, '0', null, null, null, null, null, null, '2019-06-17 15:06:55');
INSERT INTO `base_member` VALUES ('2', '2324', '0', null, null, null, '234', null, null, null, null, '2343', null, null, '234', null, null, null, '0', null, null, null, 'ecedffa6-87af-488d-b617-148cc4576713', 'e5b4a5c4-11e3-4606-959d-e5391c758c47', null, '2019-06-20 16:28:56');
INSERT INTO `base_member` VALUES ('3', '11', '0', null, null, null, '324', null, null, null, null, '232', null, null, '234', null, null, null, '0', null, null, null, '48917d87-556a-4fc3-8ee2-ba19f2dd9953', 'e5b4a5c4-11e3-4606-959d-e5391c758c47', null, '2019-06-20 16:29:51');
INSERT INTO `base_member` VALUES ('4', '', '0', null, null, null, '', null, null, null, null, '', null, null, '', null, null, null, '0', null, null, null, '1d35b45b-3c07-482d-973b-b79290ece753', 'e5b4a5c4-11e3-4606-959d-e5391c758c47', null, '2019-06-20 16:32:15');
INSERT INTO `base_member` VALUES ('5', '', '0', null, null, null, '', null, null, null, null, '', null, null, '', null, null, null, '0', null, null, null, 'df972a26-ba05-4d34-aad8-d340231fb2a6', 'null', null, '2019-06-20 16:32:20');
INSERT INTO `base_member` VALUES ('6', '', '0', null, null, null, '', null, null, null, null, '', null, null, '', null, null, null, '0', null, null, null, '1b21dcf4-4875-47ba-bdbd-809c506b52a3', 'e5b4a5c4-11e3-4606-959d-e5391c758c47', '11', '2019-06-20 16:33:38');
INSERT INTO `base_member` VALUES ('7', '大海', '1', null, null, null, '98789789', null, null, null, null, '15080154661', null, null, '171200678', null, 'dh', null, '0', null, null, null, '5fa0ad9e-a650-497f-9efb-d31fbe65d2bc', '3f1ed4e0-3eda-4f1d-b911-3ef4ca88cef8', '员工组', '2019-06-21 17:39:05');
INSERT INTO `base_member` VALUES ('8', '11', '0', null, null, null, '', null, null, null, null, '22', null, null, '', null, '11', null, '0', null, null, null, 'e78bee22-a148-4b5a-ba39-533522189af6', null, '', '2019-07-01 17:05:18');

-- ----------------------------
-- Table structure for `base_swipe_card_record`
-- ----------------------------
DROP TABLE IF EXISTS `base_swipe_card_record`;
CREATE TABLE `base_swipe_card_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '门禁、验证机刷卡记录表',
  `cardNum` varchar(20) DEFAULT NULL COMMENT '卡号',
  `swingCardTime` datetime DEFAULT NULL COMMENT '刷卡时间',
  `type` int(11) DEFAULT NULL COMMENT '刷卡人类型（0：员工 1：访客）',
  `cardholderId` int(11) DEFAULT NULL COMMENT '刷卡人ID（员工或来访人员ID）',
  `egNum` varchar(20) DEFAULT NULL COMMENT '门禁编号',
  `proofMachineName` varchar(20) DEFAULT NULL COMMENT '验证机名称',
  `inOutFlag` int(11) DEFAULT '1' COMMENT '进出门标记（0出，1进，13进 24出 默认进门））',
  `isOpenDoor` int(2) DEFAULT '0' COMMENT '0：未接门禁 1：开门成功 2：开门失败 3验证通过 4验证失败',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='门禁、验证机刷卡记录表';

-- ----------------------------
-- Records of base_swipe_card_record
-- ----------------------------

-- ----------------------------
-- Table structure for `base_takeout_res`
-- ----------------------------
DROP TABLE IF EXISTS `base_takeout_res`;
CREATE TABLE `base_takeout_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '带出物品记录ID',
  `resName` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '带出物品名称',
  `num` int(4) DEFAULT NULL COMMENT '带出物品数量',
  `pic` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '带出物品照片路径',
  `arID` int(11) DEFAULT NULL COMMENT '访客记录ID',
  `companyCode` varchar(12) DEFAULT NULL COMMENT '公司编码',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COMMENT='访客带出物品表';

-- ----------------------------
-- Records of base_takeout_res
-- ----------------------------
INSERT INTO `base_takeout_res` VALUES ('1', '证件', null, null, '1657', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('2', '钱包', null, null, '1669', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('3', '仪器', null, null, '1910', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('4', '仪器', null, null, '1908', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('5', '仪器', null, null, '1915', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('6', '钱包', null, null, '1921', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('7', '文件', null, null, '1921', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('8', '仪器', null, null, '1921', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('9', '其他', null, null, '1921', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('10', '电脑', null, null, '1921', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('11', '工具', null, null, '1921', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('12', '证件', null, null, '1921', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('13', '文件', null, null, '1926', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('14', '钱包', null, null, '1933', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('15', '文件', null, null, '1933', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('16', '仪器', null, null, '1933', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('17', '其他', null, null, '1933', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('18', '电脑', null, null, '1933', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('19', '证件', null, null, '1933', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('20', '仪器', null, null, '2288', null, '2017-12-14 13:30:02', '2017-12-14 13:30:02');
INSERT INTO `base_takeout_res` VALUES ('21', '仪器', null, null, '2555', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('22', '文件', null, null, '2712', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('23', '文件', null, null, '2723', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('24', '钱包', null, null, '2722', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('25', '文件', null, null, '2911', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('26', '钱包', null, null, '3351', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('27', '仪器', null, null, '3445', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('28', '仪器', null, null, '3535', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('29', '钱包', null, null, '3678', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('30', '文件', null, null, '3678', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('31', '仪器', null, null, '3678', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('32', '其他', null, null, '3678', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('33', '电脑', null, null, '3678', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('34', '工具', null, null, '3678', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('35', '证件', null, null, '3678', null, '2017-12-14 13:32:01', '2017-12-14 13:32:01');
INSERT INTO `base_takeout_res` VALUES ('36', '钱包', null, null, '3817', null, '2017-12-22 14:32:00', '2017-12-22 14:32:00');
INSERT INTO `base_takeout_res` VALUES ('37', '电脑', null, null, '3817', null, '2017-12-22 14:32:00', '2017-12-22 14:32:00');
INSERT INTO `base_takeout_res` VALUES ('38', '证件', null, null, '3817', null, '2017-12-22 14:32:00', '2017-12-22 14:32:00');
INSERT INTO `base_takeout_res` VALUES ('39', '大海测试11', null, null, '3817', null, '2017-12-26 11:10:00', '2017-12-26 11:10:00');

-- ----------------------------
-- Table structure for `base_visitor`
-- ----------------------------
DROP TABLE IF EXISTS `base_visitor`;
CREATE TABLE `base_visitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访客ID',
  `name` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '访客名称',
  `sex` varchar(5) DEFAULT NULL COMMENT '性别（男、女、其他）',
  `nation` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '民族',
  `birthday` date DEFAULT NULL COMMENT '出生年月日',
  `address` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '住址',
  `idnum` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '身份证号',
  `photo` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '身份证照片路径（以身份证号为照片名称）',
  `issuing` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '签发机关',
  `validityDateStart` date DEFAULT NULL COMMENT '有效期，开始时间',
  `validityDateEnd` varchar(12) DEFAULT NULL COMMENT '有效期，结束时间',
  `phone` varchar(15) DEFAULT NULL COMMENT '访客手机号码',
  `pinYin` varchar(20) DEFAULT NULL COMMENT '姓名拼音首字母（小写）',
  `code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id_companyCode` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100023 DEFAULT CHARSET=utf8mb4 COMMENT='访客信息表';

-- ----------------------------
-- Records of base_visitor
-- ----------------------------
INSERT INTO `base_visitor` VALUES ('1', '于祥楠', '男', null, null, null, '13020092004', null, null, null, null, '13020092004', 'yxn', null);
INSERT INTO `base_visitor` VALUES ('100010', '1', '男', null, null, null, '1', null, null, null, null, null, null, null);
INSERT INTO `base_visitor` VALUES ('100011', '2', '男', null, null, null, '2', null, null, null, null, null, null, null);
INSERT INTO `base_visitor` VALUES ('100012', '3', '男', null, null, null, '3', null, null, null, null, null, null, null);
INSERT INTO `base_visitor` VALUES ('100013', '4', '男', null, null, null, '4', null, null, null, null, null, null, null);
INSERT INTO `base_visitor` VALUES ('100014', '5', '男', null, null, null, '5', null, null, null, null, null, null, null);
INSERT INTO `base_visitor` VALUES ('100022', '谢金发', '男', '汉', '1998-10-24', '河北省保定市定兴县天宫寺乡南马坊村一区60号', '130626199810248179', 'visitorPhoto\\2019-07-01\\130626199810248179\\\\130626199810248179_1561974745453.png', '定兴县公安局', '2014-09-01', '2019-09-01', '13245677765', null, null);

-- ----------------------------
-- Table structure for `base_visit_reasons`
-- ----------------------------
DROP TABLE IF EXISTS `base_visit_reasons`;
CREATE TABLE `base_visit_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '来访事由选项表id',
  `value` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '来访事由',
  `isuse` int(11) DEFAULT '0' COMMENT '是否使用（0：使用 1：不使用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='来访事由选项表';

-- ----------------------------
-- Records of base_visit_reasons
-- ----------------------------
INSERT INTO `base_visit_reasons` VALUES ('7', '商务洽谈', '1');
INSERT INTO `base_visit_reasons` VALUES ('8', '工作对接', '0');
INSERT INTO `base_visit_reasons` VALUES ('9', '会议', '0');
INSERT INTO `base_visit_reasons` VALUES ('10', '其他', '0');

-- ----------------------------
-- Table structure for `base_whitelist`
-- ----------------------------
DROP TABLE IF EXISTS `base_whitelist`;
CREATE TABLE `base_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vId` int(11) NOT NULL COMMENT '访客ID',
  `reason` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '访客黑名单原因',
  `companyCode` varchar(12) DEFAULT NULL COMMENT '公司编码',
  `tabTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '标记时间（当数据更新时自动更新）',
  `uploadTime` datetime DEFAULT NULL COMMENT '上传标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COMMENT='访客黑名单表';

-- ----------------------------
-- Records of base_whitelist
-- ----------------------------
INSERT INTO `base_whitelist` VALUES ('14', '13', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('15', '14', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('16', '15', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('17', '16', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('18', '17', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('19', '18', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('20', '19', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('21', '20', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('22', '21', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('24', '114', null, null, '2017-12-08 11:00:01', '2017-12-08 11:00:01');
INSERT INTO `base_whitelist` VALUES ('25', '57', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('27', '115', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('28', '107', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('29', '34', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('30', '116', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('31', '120', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('32', '122', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('36', '533', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('37', '619', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('38', '617', null, null, '2017-12-08 11:05:00', '2017-12-08 11:05:00');
INSERT INTO `base_whitelist` VALUES ('39', '620', null, null, '2017-12-08 11:14:00', '2017-12-08 11:14:00');
INSERT INTO `base_whitelist` VALUES ('41', '1197', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_whitelist` VALUES ('42', '1199', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_whitelist` VALUES ('43', '1200', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_whitelist` VALUES ('44', '1043', null, null, '2017-12-14 11:48:01', '2017-12-14 11:48:01');
INSERT INTO `base_whitelist` VALUES ('47', '1111', null, null, '2017-12-18 10:02:00', '2017-12-18 10:02:00');
INSERT INTO `base_whitelist` VALUES ('48', '1271', null, null, '2017-12-22 13:50:00', '2017-12-22 13:50:00');
INSERT INTO `base_whitelist` VALUES ('49', '1278', null, null, '2017-12-22 15:08:00', '2017-12-22 15:08:00');
INSERT INTO `base_whitelist` VALUES ('50', '33', '大海测试黑名单', null, '2017-12-26 11:01:00', '2017-12-26 11:01:00');

-- ----------------------------
-- Table structure for `control_device_group`
-- ----------------------------
DROP TABLE IF EXISTS `control_device_group`;
CREATE TABLE `control_device_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `group_describe` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `aaa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_device_group
-- ----------------------------

-- ----------------------------
-- Table structure for `control_device_group_detail`
-- ----------------------------
DROP TABLE IF EXISTS `control_device_group_detail`;
CREATE TABLE `control_device_group_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) DEFAULT NULL,
  `device_identifier` varchar(255) DEFAULT NULL,
  `device_type` int(2) DEFAULT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_device_group_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `control_filter`
-- ----------------------------
DROP TABLE IF EXISTS `control_filter`;
CREATE TABLE `control_filter` (
  `id` int(11) DEFAULT NULL,
  `filter_config` int(2) DEFAULT NULL COMMENT '过滤器值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_filter
-- ----------------------------

-- ----------------------------
-- Table structure for `control_key`
-- ----------------------------
DROP TABLE IF EXISTS `control_key`;
CREATE TABLE `control_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_name` varchar(255) DEFAULT NULL COMMENT 'key名称',
  `biometrics` char(16) DEFAULT NULL COMMENT '生物特征',
  `card` char(16) DEFAULT NULL COMMENT '卡特征',
  `key_describe` varchar(255) DEFAULT NULL COMMENT '描述',
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_key
-- ----------------------------
INSERT INTO `control_key` VALUES ('1', '人脸和身份证', '1000000000000000', '1000000000000000', '人脸+身份证', '2019-06-14 13:41:13');
INSERT INTO `control_key` VALUES ('2', null, 'on,on,on,on', null, null, null);
INSERT INTO `control_key` VALUES ('3', null, 'on,on', null, null, null);
INSERT INTO `control_key` VALUES ('4', '111', '1,3', null, '222', null);
INSERT INTO `control_key` VALUES ('5', '777', '1000100000000000', null, '888', null);
INSERT INTO `control_key` VALUES ('6', '111', '0101000000000000', null, '222', null);
INSERT INTO `control_key` VALUES ('7', '111', '1010000000000000', null, '222', null);
INSERT INTO `control_key` VALUES ('8', '门', '1010000000000000', '1011000000000000', '一般', '2019-06-18 17:09:47');
INSERT INTO `control_key` VALUES ('9', '来了', '1101000000000000', '1101000000000000', '等等', '2019-06-18 17:15:34');
INSERT INTO `control_key` VALUES ('10', '11', '1000000000000000', '1000000000000000', '22', '2019-06-18 17:24:03');
INSERT INTO `control_key` VALUES ('11', '人脸+ic卡', '1000000000000000', '000100000000000', '支持人脸和ic卡认证', '2019-06-21 16:41:57');
INSERT INTO `control_key` VALUES ('12', '111', '1000000000000000', '1000000000000000', '22', '2019-07-01 17:46:38');

-- ----------------------------
-- Table structure for `control_path`
-- ----------------------------
DROP TABLE IF EXISTS `control_path`;
CREATE TABLE `control_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path_name` varchar(50) DEFAULT NULL COMMENT '路径名称',
  `path_describe` varchar(200) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_path
-- ----------------------------
INSERT INTO `control_path` VALUES ('3', '员工路线', '员工路线', '2019-06-21 17:37:20');
INSERT INTO `control_path` VALUES ('4', 'ceshi', 'ce', '2019-06-30 15:44:31');
INSERT INTO `control_path` VALUES ('5', '1', '2', '2019-07-01 17:50:01');

-- ----------------------------
-- Table structure for `control_path_detail`
-- ----------------------------
DROP TABLE IF EXISTS `control_path_detail`;
CREATE TABLE `control_path_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path_id` varchar(255) DEFAULT NULL COMMENT '路径id',
  `device_identifier` varchar(255) DEFAULT NULL COMMENT '设备唯一标识',
  `device_name` varchar(255) DEFAULT NULL COMMENT '设备名称',
  `in_or_out` int(2) DEFAULT NULL COMMENT '0不限 1 进 2 出',
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_path_detail
-- ----------------------------
INSERT INTO `control_path_detail` VALUES ('3', '3', '233114270', null, '0', '2019-06-21 18:25:05');
INSERT INTO `control_path_detail` VALUES ('4', '4', 'BK-8820T261000551', null, '1', '2019-06-30 15:44:48');
INSERT INTO `control_path_detail` VALUES ('5', '5', 'BK-8820T261000551', null, '1', '2019-07-01 17:50:05');

-- ----------------------------
-- Table structure for `control_person_group`
-- ----------------------------
DROP TABLE IF EXISTS `control_person_group`;
CREATE TABLE `control_person_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL COMMENT '组名称',
  `group_identifier` varchar(50) DEFAULT NULL COMMENT '组标识',
  `key_id` int(11) DEFAULT NULL COMMENT 'key主键',
  `rule_id` int(11) DEFAULT NULL COMMENT '规则主键',
  `path_id` int(11) DEFAULT NULL COMMENT '路径主键',
  `group_describe` varchar(255) DEFAULT NULL COMMENT '描述',
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_person_group
-- ----------------------------
INSERT INTO `control_person_group` VALUES ('1', '111', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('2', '222', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('3', '333', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('4', '444', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('5', '555', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('6', '666', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('7', '777', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('8', '888', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('9', '999', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('10', '000', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('11', '1222', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('12', '434', null, null, null, null, null, null);
INSERT INTO `control_person_group` VALUES ('13', 'asdf', null, null, null, null, 'sdf', null);
INSERT INTO `control_person_group` VALUES ('14', 'asdf', null, null, null, null, 'sdf', null);
INSERT INTO `control_person_group` VALUES ('15', '11', null, null, null, null, '22', null);
INSERT INTO `control_person_group` VALUES ('16', '11', null, null, null, null, '22', null);
INSERT INTO `control_person_group` VALUES ('17', '11', null, null, null, null, '22', null);
INSERT INTO `control_person_group` VALUES ('18', '撒点粉', null, null, null, null, '对方', null);
INSERT INTO `control_person_group` VALUES ('19', '为任务吧', null, null, null, null, '撒点粉', null);
INSERT INTO `control_person_group` VALUES ('20', '77777', null, '1', '2', '3', '66666', null);
INSERT INTO `control_person_group` VALUES ('21', '11', null, '10', '3', '2', '22', '2019-06-19 16:33:15');
INSERT INTO `control_person_group` VALUES ('22', '22', null, '10', '3', '2', 'c43787c9-d3a1-48ca-bf92-57b9cdee29be', '2019-06-19 17:24:53');
INSERT INTO `control_person_group` VALUES ('23', '11', 'e5b4a5c4-11e3-4606-959d-e5391c758c47', '10', '3', '2', '22', '2019-06-19 17:26:53');
INSERT INTO `control_person_group` VALUES ('24', '员工组', '3f1ed4e0-3eda-4f1d-b911-3ef4ca88cef8', '11', '1', '3', '员工组', '2019-06-21 17:38:26');
INSERT INTO `control_person_group` VALUES ('25', '11c', 'e5acaffd-9384-4f33-8755-96d98db9b076', '11', '3', '4', '22', '2019-07-01 17:43:32');

-- ----------------------------
-- Table structure for `control_person_group_detail`
-- ----------------------------
DROP TABLE IF EXISTS `control_person_group_detail`;
CREATE TABLE `control_person_group_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_group_id` int(11) DEFAULT NULL COMMENT '组id',
  `person_identifier` varchar(255) DEFAULT NULL COMMENT '人的唯一标识',
  `person_type` int(2) DEFAULT NULL COMMENT '人员类型 0 访客 1 员工 2 vip',
  `describe` varchar(255) DEFAULT NULL COMMENT '描述',
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_person_group_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `control_rule`
-- ----------------------------
DROP TABLE IF EXISTS `control_rule`;
CREATE TABLE `control_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(255) DEFAULT NULL COMMENT '规则名称',
  `rule` char(32) DEFAULT NULL COMMENT '规则',
  `rule_describe` varchar(255) DEFAULT NULL COMMENT '描述',
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of control_rule
-- ----------------------------
INSERT INTO `control_rule` VALUES ('1', '人证合一', '1100000000000000000000000000', '人和证都必须验证通过', '2019-06-14 13:41:13');
INSERT INTO `control_rule` VALUES ('2', '验证人脸', '0100000000000000000000000000', '只验证人脸', '2019-06-14 14:29:59');
INSERT INTO `control_rule` VALUES ('3', '测试', '10100000000000000000000000000000', '说明', '2019-06-19 10:32:59');
INSERT INTO `control_rule` VALUES ('4', '1', '10000000000000000000000000000000', '2', '2019-07-01 17:47:50');

-- ----------------------------
-- Table structure for `device_ai`
-- ----------------------------
DROP TABLE IF EXISTS `device_ai`;
CREATE TABLE `device_ai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_code` varchar(20) DEFAULT NULL COMMENT '设备标识',
  `ip` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `port` varchar(10) DEFAULT NULL COMMENT '端口',
  `name` varchar(20) DEFAULT NULL COMMENT '设备名称',
  `model` varchar(30) DEFAULT NULL COMMENT '型号',
  `producer` varchar(30) DEFAULT NULL COMMENT '设备提供商',
  `useTime` datetime DEFAULT NULL COMMENT '使用时间',
  `position` varchar(200) DEFAULT NULL COMMENT '使用位置',
  `gis` varchar(255) DEFAULT NULL COMMENT '经纬度',
  `manager` varchar(10) DEFAULT NULL COMMENT '设备负责人',
  `createDate` datetime DEFAULT NULL COMMENT '记录创建时间',
  `online_status` int(11) DEFAULT NULL COMMENT '1 在线 0 离线',
  `run_status` int(11) DEFAULT NULL COMMENT '1 正常 0 故障',
  `last_online_time` datetime DEFAULT NULL COMMENT '最后一次在线时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_ai
-- ----------------------------
INSERT INTO `device_ai` VALUES ('7', '213102726', '192.168.99.48', null, null, null, null, null, null, 'string', null, null, null, null, null);
INSERT INTO `device_ai` VALUES ('8', '113103029', '192.168.99.24', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `device_ai` VALUES ('9', '123', null, null, null, null, null, null, null, null, null, null, '1', null, '2018-05-28 14:44:31');

-- ----------------------------
-- Table structure for `device_car`
-- ----------------------------
DROP TABLE IF EXISTS `device_car`;
CREATE TABLE `device_car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '设备名称',
  `ip` varchar(30) NOT NULL COMMENT '设备IP',
  `ascription` varchar(200) DEFAULT NULL COMMENT '归属',
  `position` varchar(20) DEFAULT NULL COMMENT '位置',
  `gis` varchar(50) DEFAULT NULL COMMENT '经纬度',
  `online_status` int(11) DEFAULT NULL COMMENT '在线状态',
  `last_online_time` datetime DEFAULT NULL COMMENT '最后一次在线时间',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_car
-- ----------------------------
INSERT INTO `device_car` VALUES ('2', '12121', '192.138.111.211', '水电费水电费1', '水电费水电费1', '10,101', null, null, null);

-- ----------------------------
-- Table structure for `device_car_record`
-- ----------------------------
DROP TABLE IF EXISTS `device_car_record`;
CREATE TABLE `device_car_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parkid` varchar(10) DEFAULT NULL COMMENT '停车场ID',
  `carNo` varchar(20) DEFAULT NULL COMMENT '车牌号',
  `direction` int(11) DEFAULT NULL COMMENT '1进门 2 出门',
  `result` int(11) DEFAULT NULL COMMENT '1比对成功 2比对失败',
  `sendDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_car_record
-- ----------------------------
INSERT INTO `device_car_record` VALUES ('1', '2', '1001', '0', '0', '2017-01-01 12:12:12');
INSERT INTO `device_car_record` VALUES ('2', '2', '1002', '0', '0', '2018-07-09 14:57:06');
INSERT INTO `device_car_record` VALUES ('3', '2', '1003', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('4', '2', '20000', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('5', '2', '1', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('6', '2', '1', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('7', '2', '1', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('8', '2', '1', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('9', '2', '1', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('10', '2', '1', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('11', '2', '1', '0', '0', '2018-07-07 14:57:10');
INSERT INTO `device_car_record` VALUES ('12', '2', '1', '0', '0', null);

-- ----------------------------
-- Table structure for `device_entrance`
-- ----------------------------
DROP TABLE IF EXISTS `device_entrance`;
CREATE TABLE `device_entrance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(20) DEFAULT NULL COMMENT '设备标识',
  `ip` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `port` varchar(10) DEFAULT NULL COMMENT '端口',
  `name` varchar(20) DEFAULT NULL COMMENT '设备名称',
  `driver` varchar(100) DEFAULT NULL COMMENT '驱动',
  `model` varchar(30) DEFAULT NULL COMMENT '型号',
  `producer` varchar(30) DEFAULT NULL COMMENT '设备提供商',
  `useTime` datetime DEFAULT NULL COMMENT '使用时间',
  `position` varchar(200) DEFAULT NULL COMMENT '使用位置',
  `gis` varchar(255) DEFAULT NULL COMMENT '经纬度',
  `manager` varchar(10) DEFAULT NULL COMMENT '设备负责人',
  `createDate` datetime DEFAULT NULL COMMENT '记录创建时间',
  `online_status` int(11) DEFAULT NULL COMMENT '1 在线 0 离线',
  `run_status` int(11) DEFAULT NULL COMMENT '1 正常 0 故障',
  `last_online_time` datetime DEFAULT NULL COMMENT '最后一次在线时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_entrance
-- ----------------------------
INSERT INTO `device_entrance` VALUES ('22', 'BK-8820T261000551', '192.168.99.199', '8000', '1', null, '1', '', null, '', '', '', null, '1', null, '2018-09-17 15:53:22');
INSERT INTO `device_entrance` VALUES ('23', '213102726', '192.168.99.56', null, null, null, null, null, null, null, null, null, '2019-05-09 09:16:29', '1', '0', '2019-05-09 09:17:58');
INSERT INTO `device_entrance` VALUES ('24', '233114270', '192.168.99.212', null, null, null, null, null, null, null, null, null, '2019-06-21 13:54:27', '1', '0', '2019-07-03 17:28:21');

-- ----------------------------
-- Table structure for `device_face`
-- ----------------------------
DROP TABLE IF EXISTS `device_face`;
CREATE TABLE `device_face` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(20) DEFAULT NULL COMMENT '设备标识',
  `ip` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `port` varchar(10) DEFAULT NULL COMMENT '端口',
  `name` varchar(20) DEFAULT NULL COMMENT '设备名称',
  `driver` varchar(100) DEFAULT NULL COMMENT '驱动',
  `model` varchar(30) DEFAULT NULL COMMENT '型号 HQ 海清 HC 海康',
  `producer` varchar(30) DEFAULT NULL COMMENT '设备提供商',
  `useTime` datetime DEFAULT NULL COMMENT '使用时间',
  `position` varchar(200) DEFAULT NULL COMMENT '使用位置',
  `gis` varchar(255) DEFAULT NULL COMMENT '经纬度',
  `manager` varchar(10) DEFAULT NULL COMMENT '设备负责人',
  `createDate` datetime DEFAULT NULL COMMENT '记录创建时间',
  `online_status` int(11) DEFAULT NULL COMMENT '1 在线 0 离线',
  `run_status` int(11) DEFAULT NULL COMMENT '1 正常 0 故障',
  `last_online_time` datetime DEFAULT NULL COMMENT '最后一次在线时间',
  `brand` varchar(50) DEFAULT NULL COMMENT '品牌',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `pwd` varchar(50) DEFAULT NULL COMMENT '密码',
  `callurl` varchar(200) DEFAULT NULL COMMENT '消息回调地址',
  `inorout` int(11) DEFAULT '1' COMMENT '进出，1进，2出',
  `isAlarm` int(11) DEFAULT '0' COMMENT '开启回调，0否，1是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_face
-- ----------------------------
INSERT INTO `device_face` VALUES ('16', '1300884', '192.168.99.19', '80', 'TDX', null, 'TDX', 'TDX', null, 'wz', '1,1', 'fzr', '2018-06-05 09:40:34', '0', null, '2019-03-21 17:10:00', 'TDX', 'admin', '12345678', 'http://192.168.99.214', '1', '1');
INSERT INTO `device_face` VALUES ('17', '1234213', '192.168.99.88', null, 'TDX8', null, 'TDX8', '', null, '', '', '', null, '1', null, '2019-05-10 18:22:48', '', 'admin', '12345678', 'http://192.168.99.214:8080', '2', '1');
INSERT INTO `device_face` VALUES ('18', '100001', '192.168.99.12', null, 'tdx8b', null, 'TDX8', '', null, '', '', '', null, '1', null, '2019-05-10 18:24:12', '', 'admin', '12345678', 'http://192.168.99.214:8081/zl_device', '1', '1');

-- ----------------------------
-- Table structure for `device_fingerprint`
-- ----------------------------
DROP TABLE IF EXISTS `device_fingerprint`;
CREATE TABLE `device_fingerprint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '设备名称',
  `ip` varchar(30) NOT NULL COMMENT '设备IP',
  `port` varchar(10) NOT NULL COMMENT '端口',
  `ascription` varchar(200) DEFAULT NULL COMMENT '归属',
  `position` varchar(20) DEFAULT NULL COMMENT '位置',
  `direction` int(11) DEFAULT NULL COMMENT '方向 ： 1进 2 出',
  `gis` varchar(50) DEFAULT NULL COMMENT '经纬度',
  `online_status` int(11) DEFAULT NULL COMMENT '在线状态',
  `last_online_time` datetime DEFAULT NULL COMMENT '最后一次在线时间',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_fingerprint
-- ----------------------------
INSERT INTO `device_fingerprint` VALUES ('3', '中控指纹1', '192.168.99.206', '4370', '1111', '111', '2', '111', '0', null, null);
INSERT INTO `device_fingerprint` VALUES ('4', '中控指纹2', '192.168.99.205', '4370', null, null, '1', null, null, null, null);

-- ----------------------------
-- Table structure for `device_fingerprint_record`
-- ----------------------------
DROP TABLE IF EXISTS `device_fingerprint_record`;
CREATE TABLE `device_fingerprint_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceId` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0 为密码验证，1 为指纹验证，2 为卡验证',
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_fingerprint_record
-- ----------------------------
INSERT INTO `device_fingerprint_record` VALUES ('12', '3', '192.168.99.206', '3', '1', '2018-07-11 10:35:00');
INSERT INTO `device_fingerprint_record` VALUES ('13', '3', '192.168.99.206', '3', '1', '2018-07-11 10:35:00');
INSERT INTO `device_fingerprint_record` VALUES ('14', '4', '192.168.99.205', '100044', '1', '2018-07-11 10:41:00');
INSERT INTO `device_fingerprint_record` VALUES ('15', '3', '192.168.99.206', '3', '1', '2018-07-11 10:47:00');
INSERT INTO `device_fingerprint_record` VALUES ('16', '1', 'asdfsad', '123', '1', '2012-10-10 12:12:00');
INSERT INTO `device_fingerprint_record` VALUES ('17', '1', 'asdfsad', '123', '1', '2012-10-10 12:12:00');
INSERT INTO `device_fingerprint_record` VALUES ('18', '1', 'asdfsad', '123', '1', '2012-10-10 12:12:00');
INSERT INTO `device_fingerprint_record` VALUES ('19', '1', 'asdfsad', '123', '1', '2012-10-10 12:12:00');
INSERT INTO `device_fingerprint_record` VALUES ('20', '1', 'asdfsad', '123', '1', '2012-10-10 12:12:00');
INSERT INTO `device_fingerprint_record` VALUES ('21', '4', '192.168.99.205', '3', '1', '2018-07-13 08:53:00');
INSERT INTO `device_fingerprint_record` VALUES ('22', '4', '192.168.99.205', '3', '1', '2018-07-13 08:55:00');
INSERT INTO `device_fingerprint_record` VALUES ('23', '4', '192.168.99.205', '3', '1', '2018-07-13 09:07:00');
INSERT INTO `device_fingerprint_record` VALUES ('24', '4', '192.168.99.205', '3', '1', '2018-07-13 09:07:00');
INSERT INTO `device_fingerprint_record` VALUES ('25', '4', '192.168.99.205', '3', '1', '2018-07-13 09:07:00');
INSERT INTO `device_fingerprint_record` VALUES ('26', '4', '192.168.99.205', '100052', '1', '2018-07-13 09:29:00');
INSERT INTO `device_fingerprint_record` VALUES ('27', '4', '192.168.99.205', '96', '1', '2018-07-13 09:38:00');
INSERT INTO `device_fingerprint_record` VALUES ('28', '4', '192.168.99.205', '96', '1', '2018-07-13 09:38:00');
INSERT INTO `device_fingerprint_record` VALUES ('29', '4', '192.168.99.205', '100052', '1', '2018-07-13 09:40:00');
INSERT INTO `device_fingerprint_record` VALUES ('30', '4', '192.168.99.205', '100052', '1', '2018-07-13 09:47:00');
INSERT INTO `device_fingerprint_record` VALUES ('31', '4', '192.168.99.205', '96', '1', '2018-07-13 09:49:00');

-- ----------------------------
-- Table structure for `device_swing_card_record`
-- ----------------------------
DROP TABLE IF EXISTS `device_swing_card_record`;
CREATE TABLE `device_swing_card_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(20) DEFAULT NULL,
  `cardNo` varchar(20) DEFAULT NULL,
  `doorNo` int(11) DEFAULT NULL,
  `direction` int(11) DEFAULT NULL COMMENT '1 进 2 出',
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device_swing_card_record
-- ----------------------------
INSERT INTO `device_swing_card_record` VALUES ('6', '213102726', '2594809530', '1', '2', '2018-05-28 19:18:32');
INSERT INTO `device_swing_card_record` VALUES ('7', '213102726', '2594809530', '1', '2', '2018-05-28 19:18:46');
INSERT INTO `device_swing_card_record` VALUES ('8', '213102726', '889990222', '1', '1', '2018-05-28 19:19:10');
INSERT INTO `device_swing_card_record` VALUES ('9', '213102726', '2594809530', '1', '2', '2018-05-28 19:19:24');
INSERT INTO `device_swing_card_record` VALUES ('10', '213102726', '889990222', '1', '1', '2018-05-28 19:19:28');
INSERT INTO `device_swing_card_record` VALUES ('11', '213102726', '2594809530', '1', '2', '2018-05-28 19:19:35');
INSERT INTO `device_swing_card_record` VALUES ('12', '213102726', '2594809530', '1', '2', '2018-05-29 13:55:03');
INSERT INTO `device_swing_card_record` VALUES ('13', '113103029', '889990222', '1', '1', '2018-05-29 15:51:43');
INSERT INTO `device_swing_card_record` VALUES ('14', '113103029', '889990222', '1', '1', '2018-05-29 16:06:55');
INSERT INTO `device_swing_card_record` VALUES ('15', '233109703', '1411739', '1', '1', '2018-05-29 16:19:14');
INSERT INTO `device_swing_card_record` VALUES ('16', '113103029', '889990222', '1', '1', '2018-05-29 16:20:45');
INSERT INTO `device_swing_card_record` VALUES ('17', '233109703', '1411739', '1', '1', '2018-05-29 16:22:12');
INSERT INTO `device_swing_card_record` VALUES ('18', '113103029', '889990222', '1', '1', '2018-05-29 16:22:13');
INSERT INTO `device_swing_card_record` VALUES ('19', '113103029', '889990222', '1', '1', '2018-05-29 16:23:26');
INSERT INTO `device_swing_card_record` VALUES ('20', '233109703', '1411739', '1', '1', '2018-05-29 16:24:33');
INSERT INTO `device_swing_card_record` VALUES ('21', '113103029', '889990222', '1', '1', '2018-05-29 16:24:34');
INSERT INTO `device_swing_card_record` VALUES ('22', '233109703', '1411739', '1', '1', '2018-05-29 17:19:26');
INSERT INTO `device_swing_card_record` VALUES ('23', '113103029', '889990222', '1', '1', '2018-05-29 17:19:27');
INSERT INTO `device_swing_card_record` VALUES ('24', '233109703', '1411739', '1', '1', '2018-05-29 17:36:39');
INSERT INTO `device_swing_card_record` VALUES ('25', '113103029', '889990222', '1', '1', '2018-05-29 17:36:41');
INSERT INTO `device_swing_card_record` VALUES ('26', '233109703', '1411739', '1', '1', '2018-05-29 17:38:13');
INSERT INTO `device_swing_card_record` VALUES ('27', '113103029', '889990222', '1', '1', '2018-05-29 17:38:14');
INSERT INTO `device_swing_card_record` VALUES ('28', '233109703', '1411739', '1', '1', '2018-05-29 18:00:53');
INSERT INTO `device_swing_card_record` VALUES ('29', '233109703', '1411739', '1', '1', '2018-05-29 18:33:51');
INSERT INTO `device_swing_card_record` VALUES ('30', '113103029', '889990222', '1', '1', '2018-05-29 18:33:55');
INSERT INTO `device_swing_card_record` VALUES ('31', '233109703', '1411739', '1', '1', '2018-05-29 18:49:00');
INSERT INTO `device_swing_card_record` VALUES ('32', '233109703', '1411739', '1', '1', '2018-05-29 18:55:11');
INSERT INTO `device_swing_card_record` VALUES ('33', '233109703', '1411739', '1', '1', '2018-05-29 19:07:00');
INSERT INTO `device_swing_card_record` VALUES ('34', '233109703', '1411739', '1', '1', '2018-05-29 19:11:01');
INSERT INTO `device_swing_card_record` VALUES ('35', '233109703', '1411739', '1', '1', '2018-05-30 09:05:25');
INSERT INTO `device_swing_card_record` VALUES ('36', '233109703', '1411739', '1', '1', '2018-05-30 09:20:43');
INSERT INTO `device_swing_card_record` VALUES ('37', '233109703', '889990222', '1', '1', '2018-05-30 09:41:36');
INSERT INTO `device_swing_card_record` VALUES ('38', '233109703', '889990222', '1', '1', '2018-05-30 09:42:22');
INSERT INTO `device_swing_card_record` VALUES ('39', '233109703', '889990222', '1', '1', '2018-05-30 09:55:29');
INSERT INTO `device_swing_card_record` VALUES ('40', '233109703', '889990222', '1', '1', '2018-05-30 09:59:36');
INSERT INTO `device_swing_card_record` VALUES ('41', '223113050', '10', '2', '1', '2018-05-30 10:08:03');
INSERT INTO `device_swing_card_record` VALUES ('42', '223245866', '2593562906', '2', '1', '2018-05-30 10:13:00');
INSERT INTO `device_swing_card_record` VALUES ('43', '223245866', '889990222', '1', '1', '2018-05-30 10:13:16');
INSERT INTO `device_swing_card_record` VALUES ('44', '213102726', '2594809530', '1', '2', '2018-05-31 17:41:35');
INSERT INTO `device_swing_card_record` VALUES ('45', '213102726', '2594809530', '1', '2', '2018-05-31 17:41:57');
INSERT INTO `device_swing_card_record` VALUES ('46', '213102726', '2594809530', '1', '2', '2018-05-31 17:42:41');
INSERT INTO `device_swing_card_record` VALUES ('47', '213102726', '2594809530', '1', '2', '2018-05-31 18:14:03');
INSERT INTO `device_swing_card_record` VALUES ('48', '223245866', '10331146', '1', '2', '2018-06-05 16:03:11');
INSERT INTO `device_swing_card_record` VALUES ('49', '223245866', '19535962', '1', '2', '2018-06-08 16:22:49');
INSERT INTO `device_swing_card_record` VALUES ('50', '213102726', '2018001', '1', '1', '2018-06-12 10:32:19');
INSERT INTO `device_swing_card_record` VALUES ('51', '213102726', '2018001', '1', '1', '2018-06-12 10:34:19');
INSERT INTO `device_swing_card_record` VALUES ('52', '213102726', '350827929', '1', '1', '2018-06-12 10:37:53');
INSERT INTO `device_swing_card_record` VALUES ('53', '213102726', '350827929', '1', '1', '2018-06-12 10:38:11');
INSERT INTO `device_swing_card_record` VALUES ('54', '213102726', '350827929', '1', '1', '2018-06-12 10:38:16');
INSERT INTO `device_swing_card_record` VALUES ('55', '213102726', '2831253', '2', '1', '2018-06-13 11:21:25');
INSERT INTO `device_swing_card_record` VALUES ('56', '213102726', '2831253', '2', '1', '2018-06-13 11:24:25');
INSERT INTO `device_swing_card_record` VALUES ('57', '213102726', '2831253', '2', '1', '2018-06-13 11:24:42');
INSERT INTO `device_swing_card_record` VALUES ('58', '213102726', '2831253', '2', '1', '2018-06-13 11:24:56');
INSERT INTO `device_swing_card_record` VALUES ('59', '213102726', '5635680', '2', '1', '2018-06-13 11:25:08');
INSERT INTO `device_swing_card_record` VALUES ('60', '213102726', '5635680', '2', '1', '2018-06-13 11:25:23');
INSERT INTO `device_swing_card_record` VALUES ('61', '213102726', '2018001', '1', '1', '2018-06-13 11:25:36');
INSERT INTO `device_swing_card_record` VALUES ('62', '213102726', '2018001', '1', '1', '2018-06-13 11:26:30');
INSERT INTO `device_swing_card_record` VALUES ('63', '213102726', '5635680', '2', '1', '2018-06-13 11:26:45');
INSERT INTO `device_swing_card_record` VALUES ('64', '213102726', '2018001', '1', '1', '2018-06-13 11:26:46');
INSERT INTO `device_swing_card_record` VALUES ('65', '213102726', '2018001', '1', '1', '2018-06-13 11:31:45');
INSERT INTO `device_swing_card_record` VALUES ('66', '213102726', '350827929', '1', '1', '2018-06-13 11:32:38');
INSERT INTO `device_swing_card_record` VALUES ('67', '213102726', '350827929', '1', '1', '2018-06-13 11:33:01');
INSERT INTO `device_swing_card_record` VALUES ('68', '213102726', '12345678', '2', '1', '2018-06-13 11:34:16');
INSERT INTO `device_swing_card_record` VALUES ('69', '213102726', '12345678', '2', '1', '2018-06-13 11:34:21');
INSERT INTO `device_swing_card_record` VALUES ('70', '213102726', '2063234', '2', '1', '2018-06-13 11:35:02');
INSERT INTO `device_swing_card_record` VALUES ('71', '213102726', '2063234', '2', '1', '2018-06-13 11:35:05');
INSERT INTO `device_swing_card_record` VALUES ('72', '213102726', '350827929', '1', '1', '2018-06-13 11:36:31');
INSERT INTO `device_swing_card_record` VALUES ('73', '213102726', '2018001', '1', '1', '2018-06-13 18:33:23');
INSERT INTO `device_swing_card_record` VALUES ('74', '213102726', '1139822', '2', '1', '2018-06-13 18:33:31');
INSERT INTO `device_swing_card_record` VALUES ('75', '213102726', '1139822', '2', '1', '2018-06-13 18:33:48');
INSERT INTO `device_swing_card_record` VALUES ('76', '213102726', '5635680', '2', '1', '2018-06-13 18:34:08');
INSERT INTO `device_swing_card_record` VALUES ('77', '213102726', '1139822', '2', '1', '2018-06-13 18:34:36');
INSERT INTO `device_swing_card_record` VALUES ('78', '213102726', '1139822', '2', '1', '2018-06-13 18:38:29');
INSERT INTO `device_swing_card_record` VALUES ('79', '213102726', '2018001', '1', '1', '2018-06-13 18:38:30');
INSERT INTO `device_swing_card_record` VALUES ('80', '213102726', '1139822', '2', '1', '2018-06-13 18:38:31');
INSERT INTO `device_swing_card_record` VALUES ('81', '213102726', '1139822', '2', '1', '2018-06-13 18:38:35');
INSERT INTO `device_swing_card_record` VALUES ('82', '213102726', '1139822', '2', '1', '2018-06-13 18:38:41');
INSERT INTO `device_swing_card_record` VALUES ('83', '213102726', '1139822', '2', '1', '2018-06-13 18:38:47');
INSERT INTO `device_swing_card_record` VALUES ('84', '213102726', '1762352155', '1', '2', '2018-06-15 10:01:20');
INSERT INTO `device_swing_card_record` VALUES ('85', '213102726', '1074269832', '1', '2', '2018-06-15 10:09:55');
INSERT INTO `device_swing_card_record` VALUES ('86', '213102726', '1074269832', '1', '2', '2018-06-15 10:10:51');
INSERT INTO `device_swing_card_record` VALUES ('87', '213102726', '1762352155', '1', '2', '2018-06-15 10:29:46');
INSERT INTO `device_swing_card_record` VALUES ('88', '213102726', '1762352155', '1', '2', '2018-06-15 10:30:28');
INSERT INTO `device_swing_card_record` VALUES ('89', '213102726', '1762352155', '1', '2', '2018-06-15 10:30:34');
INSERT INTO `device_swing_card_record` VALUES ('90', '213102726', '1123579', '1', '2', '2018-06-15 10:31:06');
INSERT INTO `device_swing_card_record` VALUES ('91', '213102726', '5012090', '1', '2', '2018-06-15 10:31:56');
INSERT INTO `device_swing_card_record` VALUES ('92', '213102726', '13432485', '1', '2', '2018-06-15 10:32:03');
INSERT INTO `device_swing_card_record` VALUES ('93', '213102726', '5012090', '1', '2', '2018-06-15 10:34:01');
INSERT INTO `device_swing_card_record` VALUES ('94', '213102726', '5012090', '1', '2', '2018-06-15 10:34:29');
INSERT INTO `device_swing_card_record` VALUES ('95', '213102726', '5012090', '1', '2', '2018-06-15 10:34:38');
INSERT INTO `device_swing_card_record` VALUES ('96', '213102726', '5012090', '1', '2', '2018-06-15 10:34:43');
INSERT INTO `device_swing_card_record` VALUES ('97', '213102726', '5012090', '1', '2', '2018-06-15 10:34:48');
INSERT INTO `device_swing_card_record` VALUES ('98', '213102726', '5012090', '1', '2', '2018-06-15 10:34:54');
INSERT INTO `device_swing_card_record` VALUES ('99', '213102726', '5012090', '1', '2', '2018-06-15 10:34:59');
INSERT INTO `device_swing_card_record` VALUES ('100', '213102726', '22200154', '1', '2', '2018-06-15 10:53:00');
INSERT INTO `device_swing_card_record` VALUES ('101', '213102726', '22200154', '1', '2', '2018-06-15 10:53:07');
INSERT INTO `device_swing_card_record` VALUES ('102', '213102726', '22200154', '1', '2', '2018-06-15 10:53:14');
INSERT INTO `device_swing_card_record` VALUES ('103', '213102726', '22200154', '1', '2', '2018-06-15 10:53:19');
INSERT INTO `device_swing_card_record` VALUES ('104', '213102726', '22200154', '1', '2', '2018-06-15 10:53:25');
INSERT INTO `device_swing_card_record` VALUES ('105', '113103029', '15101411', '1', '2', '2018-06-23 18:22:12');
INSERT INTO `device_swing_card_record` VALUES ('106', '113103029', '16942682', '1', '2', '2018-06-23 18:22:45');
INSERT INTO `device_swing_card_record` VALUES ('107', '113103029', '16942682', '1', '2', '2018-06-23 18:27:42');
INSERT INTO `device_swing_card_record` VALUES ('108', '113103029', '16942682', '1', '2', '2018-06-23 18:29:03');
INSERT INTO `device_swing_card_record` VALUES ('109', '113103029', '16942682', '1', '2', '2018-06-23 18:29:59');
INSERT INTO `device_swing_card_record` VALUES ('110', '113103029', '16942682', '1', '2', '2018-06-23 18:31:03');
INSERT INTO `device_swing_card_record` VALUES ('111', '113103029', '16942682', '1', '2', '2018-06-23 18:31:30');
INSERT INTO `device_swing_card_record` VALUES ('112', '113103029', '16942682', '1', '2', '2018-06-23 18:36:09');
INSERT INTO `device_swing_card_record` VALUES ('113', '113103029', '16942682', '1', '2', '2018-06-23 18:36:11');
INSERT INTO `device_swing_card_record` VALUES ('114', '113103029', '19614110', '1', '2', '2018-06-23 18:41:36');
INSERT INTO `device_swing_card_record` VALUES ('115', '113103029', '19614110', '1', '2', '2018-06-23 18:41:46');
INSERT INTO `device_swing_card_record` VALUES ('116', '113103029', '19614110', '1', '2', '2018-06-23 18:43:33');
INSERT INTO `device_swing_card_record` VALUES ('117', '113103029', '889990222', '1', '1', '2018-06-23 18:43:34');
INSERT INTO `device_swing_card_record` VALUES ('118', '113103029', '19614110', '1', '2', '2018-06-23 18:43:50');
INSERT INTO `device_swing_card_record` VALUES ('119', '113103029', '889990222', '1', '1', '2018-06-23 18:43:51');
INSERT INTO `device_swing_card_record` VALUES ('120', '113103029', '19614110', '1', '2', '2018-06-23 18:43:56');
INSERT INTO `device_swing_card_record` VALUES ('121', '113103029', '889990222', '1', '1', '2018-06-23 18:43:57');
INSERT INTO `device_swing_card_record` VALUES ('122', '113103029', '19614110', '1', '2', '2018-06-23 18:48:33');
INSERT INTO `device_swing_card_record` VALUES ('123', '113103029', '889990222', '1', '1', '2018-06-23 18:48:35');
INSERT INTO `device_swing_card_record` VALUES ('124', '113103029', '19614110', '1', '2', '2018-06-23 18:54:43');
INSERT INTO `device_swing_card_record` VALUES ('125', '113103029', '889990222', '1', '1', '2018-06-23 18:54:45');
INSERT INTO `device_swing_card_record` VALUES ('126', '113103029', '19614110', '1', '2', '2018-06-23 18:54:47');
INSERT INTO `device_swing_card_record` VALUES ('127', '113103029', '889990222', '1', '1', '2018-06-23 18:54:48');
INSERT INTO `device_swing_card_record` VALUES ('128', '113103029', '19614110', '1', '2', '2018-06-23 18:57:18');
INSERT INTO `device_swing_card_record` VALUES ('129', '113103029', '889990222', '1', '1', '2018-06-23 18:57:19');
INSERT INTO `device_swing_card_record` VALUES ('130', '113103029', '24532153', '1', '2', '2018-06-23 19:02:13');
INSERT INTO `device_swing_card_record` VALUES ('131', '113103029', '24532153', '1', '2', '2018-06-23 19:02:14');
INSERT INTO `device_swing_card_record` VALUES ('132', '113103029', '24532153', '1', '2', '2018-06-23 19:02:17');
INSERT INTO `device_swing_card_record` VALUES ('133', '113103029', '24532153', '1', '2', '2018-06-23 19:02:18');
INSERT INTO `device_swing_card_record` VALUES ('134', '113103029', '24532153', '1', '2', '2018-06-23 19:02:31');
INSERT INTO `device_swing_card_record` VALUES ('135', '113103029', '24532153', '1', '2', '2018-06-23 19:02:32');
INSERT INTO `device_swing_card_record` VALUES ('136', '113103029', '19614110', '1', '2', '2018-06-23 19:03:14');
INSERT INTO `device_swing_card_record` VALUES ('137', '113103029', '889990222', '1', '1', '2018-06-23 19:03:15');
INSERT INTO `device_swing_card_record` VALUES ('138', '113103029', '24532153', '1', '2', '2018-06-23 19:03:34');
INSERT INTO `device_swing_card_record` VALUES ('139', '113103029', '24532153', '1', '2', '2018-06-23 19:03:37');
INSERT INTO `device_swing_card_record` VALUES ('140', '113103029', '24532153', '1', '2', '2018-06-23 19:03:56');
INSERT INTO `device_swing_card_record` VALUES ('141', '113103029', '24532153', '1', '2', '2018-06-23 19:06:39');
INSERT INTO `device_swing_card_record` VALUES ('142', '113103029', '19614110', '1', '2', '2018-06-23 19:07:01');
INSERT INTO `device_swing_card_record` VALUES ('143', '113103029', '889990222', '1', '1', '2018-06-23 19:07:02');
INSERT INTO `device_swing_card_record` VALUES ('144', '113103029', '24227254', '1', '2', '2018-06-23 19:07:17');
INSERT INTO `device_swing_card_record` VALUES ('145', '113103029', '889990222', '1', '1', '2018-06-23 19:07:18');
INSERT INTO `device_swing_card_record` VALUES ('146', '113103029', '16942682', '1', '2', '2018-06-23 19:09:49');
INSERT INTO `device_swing_card_record` VALUES ('147', '113103029', '16942682', '1', '2', '2018-06-23 19:09:50');
INSERT INTO `device_swing_card_record` VALUES ('148', '113103029', '16942682', '1', '2', '2018-06-23 19:09:51');
INSERT INTO `device_swing_card_record` VALUES ('149', '113103029', '16942682', '1', '2', '2018-06-23 19:09:52');
INSERT INTO `device_swing_card_record` VALUES ('150', '113103029', '16942682', '1', '2', '2018-06-23 19:09:54');
INSERT INTO `device_swing_card_record` VALUES ('151', '113103029', '16942682', '1', '2', '2018-06-23 19:09:55');
INSERT INTO `device_swing_card_record` VALUES ('152', '113103029', '16942682', '1', '2', '2018-06-23 19:09:56');
INSERT INTO `device_swing_card_record` VALUES ('153', '113103029', '16942682', '1', '2', '2018-06-23 19:10:33');
INSERT INTO `device_swing_card_record` VALUES ('154', '113103029', '15041242', '1', '2', '2018-06-23 19:10:49');
INSERT INTO `device_swing_card_record` VALUES ('155', '113103029', '2831253', '1', '2', '2018-06-23 19:11:07');
INSERT INTO `device_swing_card_record` VALUES ('156', '113103029', '4846298', '1', '2', '2018-06-23 19:11:39');
INSERT INTO `device_swing_card_record` VALUES ('157', '113103029', '4846298', '1', '2', '2018-06-23 19:13:39');
INSERT INTO `device_swing_card_record` VALUES ('158', '113103029', '4846298', '1', '2', '2018-06-23 19:13:41');
INSERT INTO `device_swing_card_record` VALUES ('159', '113103029', '4846298', '1', '2', '2018-06-23 19:13:43');
INSERT INTO `device_swing_card_record` VALUES ('160', '113103029', '19614110', '1', '2', '2018-06-23 19:14:01');
INSERT INTO `device_swing_card_record` VALUES ('161', '113103029', '889990222', '1', '1', '2018-06-23 19:14:02');
INSERT INTO `device_swing_card_record` VALUES ('162', '113103029', '19614110', '1', '2', '2018-06-23 19:14:04');
INSERT INTO `device_swing_card_record` VALUES ('163', '113103029', '889990222', '1', '1', '2018-06-23 19:14:05');
INSERT INTO `device_swing_card_record` VALUES ('164', '113103029', '19614110', '1', '2', '2018-06-23 19:14:11');
INSERT INTO `device_swing_card_record` VALUES ('165', '113103029', '889990222', '1', '1', '2018-06-23 19:14:13');
INSERT INTO `device_swing_card_record` VALUES ('166', '113103029', '4846298', '1', '2', '2018-06-23 19:17:31');
INSERT INTO `device_swing_card_record` VALUES ('167', '113103029', '19614110', '1', '2', '2018-06-23 19:17:35');
INSERT INTO `device_swing_card_record` VALUES ('168', '113103029', '889990222', '1', '1', '2018-06-23 19:17:36');
INSERT INTO `device_swing_card_record` VALUES ('169', '113103029', '22200064', '1', '2', '2018-06-23 19:23:51');
INSERT INTO `device_swing_card_record` VALUES ('170', '113103029', '889990222', '1', '1', '2018-06-23 19:23:52');
INSERT INTO `device_swing_card_record` VALUES ('171', '113103029', '22200064', '1', '2', '2018-06-23 19:24:01');
INSERT INTO `device_swing_card_record` VALUES ('172', '113103029', '889990222', '1', '1', '2018-06-23 19:24:02');
INSERT INTO `device_swing_card_record` VALUES ('173', '113103029', '22200065', '1', '2', '2018-06-23 19:26:14');
INSERT INTO `device_swing_card_record` VALUES ('174', '113103029', '889990222', '1', '1', '2018-06-23 19:26:15');
INSERT INTO `device_swing_card_record` VALUES ('175', '113103029', '22200065', '1', '2', '2018-06-23 19:26:43');
INSERT INTO `device_swing_card_record` VALUES ('176', '113103029', '889990222', '1', '1', '2018-06-23 19:26:45');
INSERT INTO `device_swing_card_record` VALUES ('177', '113103029', '22200066', '1', '2', '2018-06-23 19:29:16');
INSERT INTO `device_swing_card_record` VALUES ('178', '113103029', '889990222', '1', '1', '2018-06-23 19:29:17');
INSERT INTO `device_swing_card_record` VALUES ('179', '113103029', '22200066', '1', '2', '2018-06-23 19:29:26');
INSERT INTO `device_swing_card_record` VALUES ('180', '113103029', '889990222', '1', '1', '2018-06-23 19:29:27');
INSERT INTO `device_swing_card_record` VALUES ('181', '113103029', '15041242', '1', '1', '2018-06-30 17:30:16');
INSERT INTO `device_swing_card_record` VALUES ('182', '113103029', '889990222', '1', '1', '2018-06-30 17:56:37');
INSERT INTO `device_swing_card_record` VALUES ('183', '113103029', '889990222', '1', '1', '2018-06-30 17:58:11');
INSERT INTO `device_swing_card_record` VALUES ('184', '113103029', '889990222', '1', '1', '2018-06-30 17:59:56');
INSERT INTO `device_swing_card_record` VALUES ('185', '113103029', '889990222', '1', '1', '2018-06-30 18:02:46');
INSERT INTO `device_swing_card_record` VALUES ('186', '113103029', '889990222', '1', '1', '2018-06-30 18:06:14');
INSERT INTO `device_swing_card_record` VALUES ('187', '113103029', '889990222', '1', '1', '2018-06-30 18:07:18');
INSERT INTO `device_swing_card_record` VALUES ('188', '113103029', '889990222', '1', '1', '2018-06-30 18:08:01');
INSERT INTO `device_swing_card_record` VALUES ('189', '113103029', '889990222', '1', '1', '2018-06-30 18:09:04');
INSERT INTO `device_swing_card_record` VALUES ('190', '113103029', '889990222', '1', '1', '2018-06-30 18:10:07');
INSERT INTO `device_swing_card_record` VALUES ('191', '113103029', '889990222', '1', '1', '2018-06-30 18:13:52');
INSERT INTO `device_swing_card_record` VALUES ('192', '113103029', '889990222', '1', '1', '2018-06-30 18:14:35');
INSERT INTO `device_swing_card_record` VALUES ('193', '113103029', '889990222', '1', '1', '2018-06-30 18:16:41');
INSERT INTO `device_swing_card_record` VALUES ('194', '113103029', '889990222', '1', '1', '2018-06-30 18:18:51');
INSERT INTO `device_swing_card_record` VALUES ('195', '113103029', '889990222', '1', '1', '2018-06-30 18:23:11');
INSERT INTO `device_swing_card_record` VALUES ('196', '113103029', '889990222', '1', '1', '2018-06-30 18:26:59');
INSERT INTO `device_swing_card_record` VALUES ('197', '113103029', '889990222', '1', '1', '2018-06-30 18:27:57');
INSERT INTO `device_swing_card_record` VALUES ('198', '113103029', '889990222', '1', '1', '2018-06-30 18:30:17');
INSERT INTO `device_swing_card_record` VALUES ('199', '113103029', '889990222', '1', '1', '2018-06-30 18:32:29');
INSERT INTO `device_swing_card_record` VALUES ('200', '113103029', '889990222', '1', '1', '2018-06-30 18:33:44');
INSERT INTO `device_swing_card_record` VALUES ('201', '113103029', '889990222', '1', '1', '2018-06-30 18:34:39');
INSERT INTO `device_swing_card_record` VALUES ('202', '113103029', '889990222', '1', '1', '2018-06-30 18:36:46');
INSERT INTO `device_swing_card_record` VALUES ('203', '113103029', '889990222', '1', '1', '2018-06-30 18:40:59');
INSERT INTO `device_swing_card_record` VALUES ('204', '113103029', '889990222', '1', '1', '2018-06-30 18:42:40');
INSERT INTO `device_swing_card_record` VALUES ('205', '113103029', '889990222', '1', '1', '2018-06-30 18:43:57');
INSERT INTO `device_swing_card_record` VALUES ('206', '113103029', '889990222', '1', '1', '2018-06-30 18:46:03');
INSERT INTO `device_swing_card_record` VALUES ('207', '113103029', '889990222', '1', '1', '2018-06-30 18:49:50');
INSERT INTO `device_swing_card_record` VALUES ('208', '113103029', '10338741', '1', '1', '2018-07-04 11:55:14');
INSERT INTO `device_swing_card_record` VALUES ('209', '113103029', '905516', '1', '1', '2018-07-04 11:55:21');
INSERT INTO `device_swing_card_record` VALUES ('210', '113103029', '5635680', '1', '1', '2018-07-04 11:56:51');
INSERT INTO `device_swing_card_record` VALUES ('211', '113103029', '905516', '1', '1', '2018-07-04 13:41:48');
INSERT INTO `device_swing_card_record` VALUES ('212', '113103029', '803720', '1', '1', '2018-07-04 13:43:21');
INSERT INTO `device_swing_card_record` VALUES ('213', '113103029', '1139822', '1', '1', '2018-07-04 13:52:10');
INSERT INTO `device_swing_card_record` VALUES ('214', '113103029', '21704393', '1', '1', '2018-07-04 13:58:18');
INSERT INTO `device_swing_card_record` VALUES ('215', '113103029', '2008338', '1', '1', '2018-07-04 14:04:37');
INSERT INTO `device_swing_card_record` VALUES ('216', '113103029', '15041242', '1', '1', '2018-07-04 14:09:34');
INSERT INTO `device_swing_card_record` VALUES ('217', '113103029', '10338741', '1', '1', '2018-07-04 14:29:02');
INSERT INTO `device_swing_card_record` VALUES ('218', '113103029', '5635680', '1', '1', '2018-07-04 14:29:03');
INSERT INTO `device_swing_card_record` VALUES ('219', '113103029', '5635680', '1', '1', '2018-07-04 14:29:04');
INSERT INTO `device_swing_card_record` VALUES ('220', '113103029', '803720', '1', '1', '2018-07-04 14:30:41');
INSERT INTO `device_swing_card_record` VALUES ('221', '113103029', '803720', '1', '1', '2018-07-04 14:30:49');
INSERT INTO `device_swing_card_record` VALUES ('222', '113103029', '21704393', '1', '1', '2018-07-04 14:30:53');
INSERT INTO `device_swing_card_record` VALUES ('223', '113103029', '889990222', '1', '1', '2018-07-04 14:30:54');
INSERT INTO `device_swing_card_record` VALUES ('224', '113103029', '5635680', '1', '1', '2018-07-04 14:30:57');
INSERT INTO `device_swing_card_record` VALUES ('225', '113103029', '889990222', '1', '1', '2018-07-04 14:30:58');
INSERT INTO `device_swing_card_record` VALUES ('226', '113103029', '2008338', '1', '1', '2018-07-04 14:31:00');
INSERT INTO `device_swing_card_record` VALUES ('227', '113103029', '889990222', '1', '1', '2018-07-04 14:31:01');
INSERT INTO `device_swing_card_record` VALUES ('228', '113103029', '2008338', '1', '1', '2018-07-04 14:31:03');
INSERT INTO `device_swing_card_record` VALUES ('229', '113103029', '889990222', '1', '1', '2018-07-04 14:31:04');
INSERT INTO `device_swing_card_record` VALUES ('230', '113103029', '1139822', '1', '1', '2018-07-04 14:31:11');
INSERT INTO `device_swing_card_record` VALUES ('231', '113103029', '889990222', '1', '1', '2018-07-04 14:31:12');
INSERT INTO `device_swing_card_record` VALUES ('232', '113103029', '2008338', '1', '1', '2018-07-04 14:31:15');
INSERT INTO `device_swing_card_record` VALUES ('233', '113103029', '889990222', '1', '1', '2018-07-04 14:31:16');
INSERT INTO `device_swing_card_record` VALUES ('234', '113103029', '905516', '1', '1', '2018-07-04 14:31:18');
INSERT INTO `device_swing_card_record` VALUES ('235', '113103029', '21704393', '1', '1', '2018-07-04 14:31:21');
INSERT INTO `device_swing_card_record` VALUES ('236', '113103029', '889990222', '1', '1', '2018-07-04 14:31:22');
INSERT INTO `device_swing_card_record` VALUES ('237', '113103029', '2008338', '1', '1', '2018-07-04 14:31:24');
INSERT INTO `device_swing_card_record` VALUES ('238', '113103029', '889990222', '1', '1', '2018-07-04 14:31:26');
INSERT INTO `device_swing_card_record` VALUES ('239', '113103029', '2008338', '1', '1', '2018-07-04 14:31:29');
INSERT INTO `device_swing_card_record` VALUES ('240', '113103029', '889990222', '1', '1', '2018-07-04 14:31:30');
INSERT INTO `device_swing_card_record` VALUES ('241', '113103029', '2008338', '1', '1', '2018-07-04 14:31:39');
INSERT INTO `device_swing_card_record` VALUES ('242', '113103029', '889990222', '1', '1', '2018-07-04 14:31:40');
INSERT INTO `device_swing_card_record` VALUES ('243', '113103029', '2008338', '1', '1', '2018-07-04 14:31:57');
INSERT INTO `device_swing_card_record` VALUES ('244', '113103029', '889990222', '1', '1', '2018-07-04 14:31:58');
INSERT INTO `device_swing_card_record` VALUES ('245', '113103029', '905516', '1', '1', '2018-07-04 14:32:06');
INSERT INTO `device_swing_card_record` VALUES ('246', '113103029', '1139822', '1', '1', '2018-07-04 14:34:01');
INSERT INTO `device_swing_card_record` VALUES ('247', '113103029', '1139822', '1', '1', '2018-07-04 14:34:03');
INSERT INTO `device_swing_card_record` VALUES ('248', '113103029', '5635680', '1', '1', '2018-07-04 14:34:06');
INSERT INTO `device_swing_card_record` VALUES ('249', '113103029', '803720', '1', '1', '2018-07-04 14:34:08');
INSERT INTO `device_swing_card_record` VALUES ('250', '113103029', '2008338', '1', '1', '2018-07-04 14:34:12');
INSERT INTO `device_swing_card_record` VALUES ('251', '113103029', '905516', '1', '1', '2018-07-04 14:34:15');
INSERT INTO `device_swing_card_record` VALUES ('252', '113103029', '21704393', '1', '1', '2018-07-04 14:34:18');
INSERT INTO `device_swing_card_record` VALUES ('253', '113103029', '1139822', '1', '1', '2018-07-04 14:34:20');
INSERT INTO `device_swing_card_record` VALUES ('254', '113103029', '1139822', '1', '1', '2018-07-04 15:53:57');
INSERT INTO `device_swing_card_record` VALUES ('255', '113103029', '1139822', '1', '1', '2018-07-04 16:03:13');
INSERT INTO `device_swing_card_record` VALUES ('256', '113103029', '1139822', '1', '1', '2018-07-04 16:07:45');
INSERT INTO `device_swing_card_record` VALUES ('257', '113103029', '1139822', '1', '1', '2018-07-04 16:10:01');
INSERT INTO `device_swing_card_record` VALUES ('258', '113103029', '1139822', '1', '1', '2018-07-04 16:13:47');
INSERT INTO `device_swing_card_record` VALUES ('259', '113103029', '1139822', '1', '1', '2018-07-04 16:18:04');
INSERT INTO `device_swing_card_record` VALUES ('260', '113103029', '1139822', '1', '1', '2018-07-04 16:44:22');
INSERT INTO `device_swing_card_record` VALUES ('261', '113103029', '1139822', '1', '1', '2018-07-04 16:47:57');
INSERT INTO `device_swing_card_record` VALUES ('262', '113103029', '1139822', '1', '1', '2018-07-04 16:56:30');
INSERT INTO `device_swing_card_record` VALUES ('263', '113103029', '1139822', '1', '1', '2018-07-04 17:00:11');
INSERT INTO `device_swing_card_record` VALUES ('264', '113103029', '1139822', '1', '1', '2018-07-04 17:32:46');
INSERT INTO `device_swing_card_record` VALUES ('265', '113103029', '1139822', '1', '1', '2018-07-04 17:41:14');
INSERT INTO `device_swing_card_record` VALUES ('266', '113103029', '1139822', '1', '1', '2018-07-09 17:34:25');
INSERT INTO `device_swing_card_record` VALUES ('267', '113103029', '1139822', '1', '1', '2018-07-09 17:43:30');
INSERT INTO `device_swing_card_record` VALUES ('268', '113103029', '1139822', '1', '1', '2018-07-09 17:44:33');
INSERT INTO `device_swing_card_record` VALUES ('269', '113103029', '1139822', '1', '1', '2018-07-09 17:46:08');
INSERT INTO `device_swing_card_record` VALUES ('270', '113103029', '1139822', '1', '1', '2018-07-09 17:53:12');
INSERT INTO `device_swing_card_record` VALUES ('271', '113103029', '1139822', '1', '1', '2018-07-09 18:00:07');
INSERT INTO `device_swing_card_record` VALUES ('272', '113103029', '1139822', '1', '1', '2018-07-09 18:01:36');
INSERT INTO `device_swing_card_record` VALUES ('273', '113103029', '1139822', '1', '1', '2018-07-10 11:33:20');
INSERT INTO `device_swing_card_record` VALUES ('274', '113103029', '1139822', '1', '1', '2018-07-10 11:34:54');
INSERT INTO `device_swing_card_record` VALUES ('275', '113103029', '1139822', '1', '1', '2018-07-10 11:36:13');
INSERT INTO `device_swing_card_record` VALUES ('276', '113103029', '1139822', '1', '1', '2018-07-10 11:40:36');
INSERT INTO `device_swing_card_record` VALUES ('277', '113103029', '1139822', '1', '1', '2018-07-10 15:04:05');
INSERT INTO `device_swing_card_record` VALUES ('278', '113103029', '1139822', '1', '1', '2018-07-10 15:06:17');
INSERT INTO `device_swing_card_record` VALUES ('279', '113103029', '1139822', '1', '1', '2018-07-10 15:09:30');
INSERT INTO `device_swing_card_record` VALUES ('280', '113103029', '1139822', '1', '1', '2018-07-10 15:24:32');
INSERT INTO `device_swing_card_record` VALUES ('281', '113103029', '1139822', '1', '1', '2018-07-10 15:29:41');
INSERT INTO `device_swing_card_record` VALUES ('282', '113103029', '1139822', '1', '1', '2018-07-10 15:32:56');
INSERT INTO `device_swing_card_record` VALUES ('283', '113103029', '1139822', '1', '1', '2018-07-10 15:38:57');
INSERT INTO `device_swing_card_record` VALUES ('284', '113103029', '1139822', '1', '1', '2018-07-10 15:39:33');
INSERT INTO `device_swing_card_record` VALUES ('285', '113103029', '1139822', '1', '1', '2018-07-10 15:49:00');
INSERT INTO `device_swing_card_record` VALUES ('286', '113103029', '1139822', '1', '1', '2018-07-10 17:17:31');
INSERT INTO `device_swing_card_record` VALUES ('287', '113103029', '1139822', '1', '1', '2018-07-10 17:18:23');
INSERT INTO `device_swing_card_record` VALUES ('288', '113103029', '1139822', '1', '1', '2018-07-10 17:24:40');
INSERT INTO `device_swing_card_record` VALUES ('289', '113103029', '1139822', '1', '1', '2018-07-10 17:32:38');
INSERT INTO `device_swing_card_record` VALUES ('290', '113103029', '1139822', '1', '1', '2018-07-10 17:52:59');
INSERT INTO `device_swing_card_record` VALUES ('291', '113103029', '1139822', '1', '1', '2018-07-10 18:00:56');
INSERT INTO `device_swing_card_record` VALUES ('292', '113103029', '1139822', '1', '1', '2018-07-10 18:34:24');
INSERT INTO `device_swing_card_record` VALUES ('293', '113103029', '1139822', '1', '1', '2018-07-10 18:41:13');
INSERT INTO `device_swing_card_record` VALUES ('294', '113103029', '1139822', '1', '1', '2018-07-10 18:52:19');
INSERT INTO `device_swing_card_record` VALUES ('295', '113103029', '1139822', '1', '1', '2018-07-10 19:03:35');
INSERT INTO `device_swing_card_record` VALUES ('296', '113103029', '1139822', '1', '1', '2018-07-11 10:34:51');
INSERT INTO `device_swing_card_record` VALUES ('297', '113103029', '1139822', '1', '1', '2018-07-11 15:11:29');
INSERT INTO `device_swing_card_record` VALUES ('298', 'BK-8820T26100055', '11625552', '2', '1', '2018-09-17 10:03:10');
INSERT INTO `device_swing_card_record` VALUES ('299', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:34:11');
INSERT INTO `device_swing_card_record` VALUES ('300', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:34:56');
INSERT INTO `device_swing_card_record` VALUES ('301', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:35:16');
INSERT INTO `device_swing_card_record` VALUES ('302', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:36:43');
INSERT INTO `device_swing_card_record` VALUES ('303', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:37:43');
INSERT INTO `device_swing_card_record` VALUES ('304', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:42:43');
INSERT INTO `device_swing_card_record` VALUES ('305', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:43:27');
INSERT INTO `device_swing_card_record` VALUES ('306', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:43:57');
INSERT INTO `device_swing_card_record` VALUES ('307', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:47:37');
INSERT INTO `device_swing_card_record` VALUES ('308', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:53:27');
INSERT INTO `device_swing_card_record` VALUES ('309', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:54:30');
INSERT INTO `device_swing_card_record` VALUES ('310', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:55:18');
INSERT INTO `device_swing_card_record` VALUES ('311', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 10:56:46');
INSERT INTO `device_swing_card_record` VALUES ('312', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:02:12');
INSERT INTO `device_swing_card_record` VALUES ('313', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:02:35');
INSERT INTO `device_swing_card_record` VALUES ('314', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:03:32');
INSERT INTO `device_swing_card_record` VALUES ('315', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:03:50');
INSERT INTO `device_swing_card_record` VALUES ('316', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:03:58');
INSERT INTO `device_swing_card_record` VALUES ('317', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:05:25');
INSERT INTO `device_swing_card_record` VALUES ('318', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:05:51');
INSERT INTO `device_swing_card_record` VALUES ('319', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:06:36');
INSERT INTO `device_swing_card_record` VALUES ('320', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:06:52');
INSERT INTO `device_swing_card_record` VALUES ('321', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:08:09');
INSERT INTO `device_swing_card_record` VALUES ('322', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:09:03');
INSERT INTO `device_swing_card_record` VALUES ('323', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:09:10');
INSERT INTO `device_swing_card_record` VALUES ('324', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:10:37');
INSERT INTO `device_swing_card_record` VALUES ('325', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:11:43');
INSERT INTO `device_swing_card_record` VALUES ('326', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:13:06');
INSERT INTO `device_swing_card_record` VALUES ('327', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:13:14');
INSERT INTO `device_swing_card_record` VALUES ('328', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:13:18');
INSERT INTO `device_swing_card_record` VALUES ('329', 'BK-8820T26100055', '11625552', '2', '1', '2018-09-17 11:13:22');
INSERT INTO `device_swing_card_record` VALUES ('330', 'BK-8820T26100055', '11625552', '2', '1', '2018-09-17 11:13:24');
INSERT INTO `device_swing_card_record` VALUES ('331', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:13:27');
INSERT INTO `device_swing_card_record` VALUES ('332', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 11:28:25');
INSERT INTO `device_swing_card_record` VALUES ('333', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 12:08:08');
INSERT INTO `device_swing_card_record` VALUES ('334', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 12:08:13');
INSERT INTO `device_swing_card_record` VALUES ('335', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 12:08:24');
INSERT INTO `device_swing_card_record` VALUES ('336', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 12:08:42');
INSERT INTO `device_swing_card_record` VALUES ('337', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 12:56:49');
INSERT INTO `device_swing_card_record` VALUES ('338', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:02:19');
INSERT INTO `device_swing_card_record` VALUES ('339', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:13:04');
INSERT INTO `device_swing_card_record` VALUES ('340', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:14:07');
INSERT INTO `device_swing_card_record` VALUES ('341', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:14:10');
INSERT INTO `device_swing_card_record` VALUES ('342', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:17:59');
INSERT INTO `device_swing_card_record` VALUES ('343', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:19:07');
INSERT INTO `device_swing_card_record` VALUES ('344', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:21:30');
INSERT INTO `device_swing_card_record` VALUES ('345', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:23:55');
INSERT INTO `device_swing_card_record` VALUES ('346', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:32:03');
INSERT INTO `device_swing_card_record` VALUES ('347', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:32:08');
INSERT INTO `device_swing_card_record` VALUES ('348', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:32:14');
INSERT INTO `device_swing_card_record` VALUES ('349', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:32:38');
INSERT INTO `device_swing_card_record` VALUES ('350', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:32:53');
INSERT INTO `device_swing_card_record` VALUES ('351', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:32:58');
INSERT INTO `device_swing_card_record` VALUES ('352', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:33:05');
INSERT INTO `device_swing_card_record` VALUES ('353', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:33:12');
INSERT INTO `device_swing_card_record` VALUES ('354', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:34:05');
INSERT INTO `device_swing_card_record` VALUES ('355', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:34:09');
INSERT INTO `device_swing_card_record` VALUES ('356', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:41:18');
INSERT INTO `device_swing_card_record` VALUES ('357', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:41:24');
INSERT INTO `device_swing_card_record` VALUES ('358', 'BK-8820T26100055', '11625552', '2', '1', '2018-09-17 13:45:09');
INSERT INTO `device_swing_card_record` VALUES ('359', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 13:45:16');
INSERT INTO `device_swing_card_record` VALUES ('360', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 15:39:09');
INSERT INTO `device_swing_card_record` VALUES ('361', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 15:40:34');
INSERT INTO `device_swing_card_record` VALUES ('362', 'BK-8820T26100055', '3053351', '2', '1', '2018-09-17 15:43:17');
INSERT INTO `device_swing_card_record` VALUES ('363', 'BK-8820T26100055', '11625552', '2', '1', '2018-09-17 15:53:32');
INSERT INTO `device_swing_card_record` VALUES ('364', '213102726', '16455059', '1', '2', '2019-05-09 09:16:36');
INSERT INTO `device_swing_card_record` VALUES ('365', '233114270', '591750930', '2', '1', '2019-06-21 13:54:26');
INSERT INTO `device_swing_card_record` VALUES ('366', '233114270', '171200678', '2', '2', '2019-06-21 14:08:59');
INSERT INTO `device_swing_card_record` VALUES ('367', '233114270', '171200678', '2', '2', '2019-06-21 14:09:05');
INSERT INTO `device_swing_card_record` VALUES ('368', '233114270', '171200678', '2', '2', '2019-06-21 14:09:35');
INSERT INTO `device_swing_card_record` VALUES ('369', '233114270', '171200678', '2', '2', '2019-06-21 14:09:53');
INSERT INTO `device_swing_card_record` VALUES ('370', '233114270', '171200678', '2', '2', '2019-06-21 14:10:05');
INSERT INTO `device_swing_card_record` VALUES ('371', '233114270', '718884262', '2', '2', '2019-06-21 14:10:17');
INSERT INTO `device_swing_card_record` VALUES ('372', '233114270', '171200678', '2', '2', '2019-06-21 14:15:26');
INSERT INTO `device_swing_card_record` VALUES ('373', '233114270', '171200678', '2', '2', '2019-06-21 14:28:33');
INSERT INTO `device_swing_card_record` VALUES ('374', '233114270', '171200678', '2', '2', '2019-06-21 14:28:38');
INSERT INTO `device_swing_card_record` VALUES ('375', '233114270', '171200678', '2', '2', '2019-06-21 14:28:48');
INSERT INTO `device_swing_card_record` VALUES ('376', '233114270', '171200678', '2', '2', '2019-06-21 14:38:01');
INSERT INTO `device_swing_card_record` VALUES ('377', '233114270', '171200678', '2', '2', '2019-06-21 14:38:28');
INSERT INTO `device_swing_card_record` VALUES ('378', '233114270', '171200678', '2', '2', '2019-06-21 14:39:38');
INSERT INTO `device_swing_card_record` VALUES ('379', '233114270', '1529331798', '1', '1', '2019-06-21 14:39:40');
INSERT INTO `device_swing_card_record` VALUES ('380', '233114270', '171200678', '2', '2', '2019-06-21 14:39:55');
INSERT INTO `device_swing_card_record` VALUES ('381', '233114270', '1529331798', '1', '1', '2019-06-21 14:39:56');
INSERT INTO `device_swing_card_record` VALUES ('382', '233114270', '171200678', '2', '2', '2019-06-21 14:40:24');
INSERT INTO `device_swing_card_record` VALUES ('383', '233114270', '171200678', '2', '2', '2019-06-21 14:40:35');
INSERT INTO `device_swing_card_record` VALUES ('384', '233114270', '1529331798', '1', '1', '2019-06-21 14:40:36');
INSERT INTO `device_swing_card_record` VALUES ('385', '233114270', '171200678', '2', '2', '2019-06-21 14:42:00');
INSERT INTO `device_swing_card_record` VALUES ('386', '233114270', '1529331798', '1', '1', '2019-06-21 14:42:01');
INSERT INTO `device_swing_card_record` VALUES ('387', '233114270', '171200678', '2', '2', '2019-06-21 14:45:10');
INSERT INTO `device_swing_card_record` VALUES ('388', '233114270', '171200678', '2', '2', '2019-06-21 14:45:32');
INSERT INTO `device_swing_card_record` VALUES ('389', '233114270', '1529331798', '1', '1', '2019-06-21 14:45:33');
INSERT INTO `device_swing_card_record` VALUES ('390', '233114270', '171200678', '2', '2', '2019-06-21 14:48:37');
INSERT INTO `device_swing_card_record` VALUES ('391', '233114270', '1529331798', '1', '1', '2019-06-21 14:48:38');
INSERT INTO `device_swing_card_record` VALUES ('392', '233114270', '171200678', '2', '2', '2019-06-21 14:48:46');
INSERT INTO `device_swing_card_record` VALUES ('393', '233114270', '171200678', '2', '2', '2019-06-21 15:01:59');
INSERT INTO `device_swing_card_record` VALUES ('394', '233114270', '171200678', '2', '2', '2019-06-21 16:32:34');
INSERT INTO `device_swing_card_record` VALUES ('395', '233114270', '171200678', '2', '2', '2019-06-21 16:32:39');
INSERT INTO `device_swing_card_record` VALUES ('396', '233114270', '171200678', '2', '2', '2019-06-21 16:32:54');
INSERT INTO `device_swing_card_record` VALUES ('397', '233114270', '171200678', '2', '2', '2019-06-21 16:33:05');
INSERT INTO `device_swing_card_record` VALUES ('398', '233114270', '171200678', '2', '2', '2019-06-21 16:34:43');
INSERT INTO `device_swing_card_record` VALUES ('399', '233114270', '171200678', '2', '2', '2019-06-21 16:35:32');
INSERT INTO `device_swing_card_record` VALUES ('400', '233114270', '171200678', '2', '2', '2019-06-21 16:35:39');
INSERT INTO `device_swing_card_record` VALUES ('401', '233114270', '171200678', '2', '2', '2019-06-21 16:36:25');
INSERT INTO `device_swing_card_record` VALUES ('402', '233114270', '171200678', '2', '2', '2019-06-21 17:30:02');
INSERT INTO `device_swing_card_record` VALUES ('403', '233114270', '171200678', '2', '2', '2019-06-21 17:40:28');
INSERT INTO `device_swing_card_record` VALUES ('404', '233114270', '171200678', '2', '2', '2019-06-21 17:48:16');
INSERT INTO `device_swing_card_record` VALUES ('405', '233114270', '171200678', '2', '2', '2019-06-21 17:56:14');
INSERT INTO `device_swing_card_record` VALUES ('406', '233114270', '171200678', '2', '2', '2019-06-21 17:56:24');
INSERT INTO `device_swing_card_record` VALUES ('407', '233114270', '171200678', '2', '2', '2019-06-21 17:56:58');
INSERT INTO `device_swing_card_record` VALUES ('408', '233114270', '171200678', '2', '2', '2019-06-21 17:57:50');
INSERT INTO `device_swing_card_record` VALUES ('409', '233114270', '171200678', '2', '2', '2019-06-21 18:16:34');
INSERT INTO `device_swing_card_record` VALUES ('410', '233114270', '171200678', '2', '2', '2019-06-21 18:16:41');
INSERT INTO `device_swing_card_record` VALUES ('411', '233114270', '171200678', '2', '2', '2019-06-21 18:17:02');
INSERT INTO `device_swing_card_record` VALUES ('412', '233114270', '171200678', '2', '2', '2019-06-21 18:17:20');
INSERT INTO `device_swing_card_record` VALUES ('413', '233114270', '171200678', '2', '2', '2019-06-21 18:34:57');
INSERT INTO `device_swing_card_record` VALUES ('414', '233114270', '171200678', '2', '2', '2019-06-21 18:35:49');
INSERT INTO `device_swing_card_record` VALUES ('415', '233114270', '171200678', '2', '2', '2019-06-21 18:35:52');
INSERT INTO `device_swing_card_record` VALUES ('416', '233114270', '171200678', '2', '2', '2019-06-21 18:36:44');
INSERT INTO `device_swing_card_record` VALUES ('417', '233114270', '171200678', '2', '2', '2019-06-21 18:37:28');
INSERT INTO `device_swing_card_record` VALUES ('418', '233114270', '1529331798', '1', '1', '2019-06-21 18:37:29');
INSERT INTO `device_swing_card_record` VALUES ('419', '233114270', '171200678', '2', '2', '2019-06-21 18:37:38');
INSERT INTO `device_swing_card_record` VALUES ('420', '233114270', '1529331798', '1', '1', '2019-06-21 18:37:40');
INSERT INTO `device_swing_card_record` VALUES ('421', '233114270', '171200678', '2', '2', '2019-06-21 18:38:27');
INSERT INTO `device_swing_card_record` VALUES ('422', '233114270', '1529331798', '1', '1', '2019-06-21 18:38:28');
INSERT INTO `device_swing_card_record` VALUES ('423', '233114270', '718884262', '2', '2', '2019-06-21 18:38:31');
INSERT INTO `device_swing_card_record` VALUES ('424', '233114270', '718884262', '2', '2', '2019-06-21 18:38:36');
INSERT INTO `device_swing_card_record` VALUES ('425', '233114270', '171200678', '2', '2', '2019-06-21 18:38:39');
INSERT INTO `device_swing_card_record` VALUES ('426', '233114270', '1529331798', '1', '1', '2019-06-21 18:38:40');
INSERT INTO `device_swing_card_record` VALUES ('427', '233114270', '171200678', '2', '2', '2019-06-21 18:51:56');
INSERT INTO `device_swing_card_record` VALUES ('428', '233114270', '171200678', '2', '2', '2019-06-21 18:51:59');
INSERT INTO `device_swing_card_record` VALUES ('429', '233114270', '171200678', '2', '2', '2019-06-21 18:55:51');
INSERT INTO `device_swing_card_record` VALUES ('430', '233114270', '171200678', '2', '2', '2019-06-21 18:55:54');
INSERT INTO `device_swing_card_record` VALUES ('431', '233114270', '171200678', '2', '2', '2019-06-21 18:55:57');
INSERT INTO `device_swing_card_record` VALUES ('432', '233114270', '171200678', '2', '2', '2019-06-21 18:56:15');
INSERT INTO `device_swing_card_record` VALUES ('433', '233114270', '1529331798', '1', '1', '2019-06-21 18:56:17');
INSERT INTO `device_swing_card_record` VALUES ('434', '233114270', '171200678', '2', '2', '2019-06-21 18:56:22');
INSERT INTO `device_swing_card_record` VALUES ('435', '233114270', '1529331798', '1', '1', '2019-06-21 18:56:23');
INSERT INTO `device_swing_card_record` VALUES ('436', '233114270', '171200678', '2', '2', '2019-06-21 18:56:36');
INSERT INTO `device_swing_card_record` VALUES ('437', '233114270', '1529331798', '1', '1', '2019-06-21 18:56:38');
INSERT INTO `device_swing_card_record` VALUES ('438', '233114270', '171200678', '2', '2', '2019-06-21 18:56:44');
INSERT INTO `device_swing_card_record` VALUES ('439', '233114270', '1529331798', '1', '1', '2019-06-21 18:56:45');
INSERT INTO `device_swing_card_record` VALUES ('440', '233114270', '171200678', '2', '2', '2019-06-21 18:58:04');
INSERT INTO `device_swing_card_record` VALUES ('441', '233114270', '1529331798', '1', '1', '2019-06-21 18:58:05');
INSERT INTO `device_swing_card_record` VALUES ('442', '233114270', '171200678', '2', '2', '2019-06-21 18:58:09');
INSERT INTO `device_swing_card_record` VALUES ('443', '233114270', '1529331798', '1', '1', '2019-06-21 18:58:10');
INSERT INTO `device_swing_card_record` VALUES ('444', '233114270', '171200678', '2', '2', '2019-06-21 18:58:31');
INSERT INTO `device_swing_card_record` VALUES ('445', '233114270', '1529331798', '1', '1', '2019-06-21 18:58:32');
INSERT INTO `device_swing_card_record` VALUES ('446', '233114270', '171200678', '2', '2', '2019-06-21 18:58:34');
INSERT INTO `device_swing_card_record` VALUES ('447', '233114270', '1529331798', '1', '1', '2019-06-21 18:58:35');
INSERT INTO `device_swing_card_record` VALUES ('448', '233114270', '171200678', '2', '2', '2019-06-21 19:00:23');
INSERT INTO `device_swing_card_record` VALUES ('449', '233114270', '1529331798', '1', '1', '2019-06-21 19:00:25');
INSERT INTO `device_swing_card_record` VALUES ('450', '233114270', '171200678', '2', '2', '2019-06-21 19:00:28');
INSERT INTO `device_swing_card_record` VALUES ('451', '233114270', '1529331798', '1', '1', '2019-06-21 19:00:29');
INSERT INTO `device_swing_card_record` VALUES ('452', '233114270', '171200678', '2', '2', '2019-06-21 19:01:22');
INSERT INTO `device_swing_card_record` VALUES ('453', '233114270', '1529331798', '1', '1', '2019-06-21 19:01:23');
INSERT INTO `device_swing_card_record` VALUES ('454', '233114270', '171200678', '2', '2', '2019-06-21 19:01:26');
INSERT INTO `device_swing_card_record` VALUES ('455', '233114270', '1529331798', '1', '1', '2019-06-21 19:01:28');
INSERT INTO `device_swing_card_record` VALUES ('456', '233114270', '171200678', '2', '2', '2019-06-21 19:01:50');
INSERT INTO `device_swing_card_record` VALUES ('457', '233114270', '1529331798', '1', '1', '2019-06-21 19:01:51');
INSERT INTO `device_swing_card_record` VALUES ('458', '233114270', '171200678', '2', '2', '2019-06-21 19:01:55');
INSERT INTO `device_swing_card_record` VALUES ('459', '233114270', '1529331798', '1', '1', '2019-06-21 19:01:56');
INSERT INTO `device_swing_card_record` VALUES ('460', '233114270', '171200678', '2', '2', '2019-06-21 19:02:12');
INSERT INTO `device_swing_card_record` VALUES ('461', '233114270', '1529331798', '1', '1', '2019-06-21 19:02:14');
INSERT INTO `device_swing_card_record` VALUES ('462', '233114270', '171200678', '2', '2', '2019-06-21 19:05:13');
INSERT INTO `device_swing_card_record` VALUES ('463', '233114270', '1529331798', '1', '1', '2019-06-21 19:05:15');
INSERT INTO `device_swing_card_record` VALUES ('464', '233114270', '171200678', '2', '2', '2019-06-21 19:05:49');
INSERT INTO `device_swing_card_record` VALUES ('465', '233114270', '1529331798', '1', '1', '2019-06-21 19:05:50');
INSERT INTO `device_swing_card_record` VALUES ('466', '233114270', '171200678', '2', '2', '2019-06-21 19:06:04');
INSERT INTO `device_swing_card_record` VALUES ('467', '233114270', '1529331798', '1', '1', '2019-06-21 19:06:05');
INSERT INTO `device_swing_card_record` VALUES ('468', '233114270', '171200678', '2', '2', '2019-06-21 19:06:07');
INSERT INTO `device_swing_card_record` VALUES ('469', '233114270', '1529331798', '1', '1', '2019-06-21 19:06:08');
INSERT INTO `device_swing_card_record` VALUES ('470', '233114270', '171200678', '2', '2', '2019-06-21 19:06:09');
INSERT INTO `device_swing_card_record` VALUES ('471', '233114270', '1529331798', '1', '1', '2019-06-21 19:06:11');
INSERT INTO `device_swing_card_record` VALUES ('472', '233114270', '171200678', '2', '2', '2019-06-21 19:06:12');
INSERT INTO `device_swing_card_record` VALUES ('473', '233114270', '1529331798', '1', '1', '2019-06-21 19:06:14');
INSERT INTO `device_swing_card_record` VALUES ('474', '233114270', '171200678', '2', '2', '2019-06-21 19:06:15');
INSERT INTO `device_swing_card_record` VALUES ('475', '233114270', '1529331798', '1', '1', '2019-06-21 19:06:17');
INSERT INTO `device_swing_card_record` VALUES ('476', '233114270', '171200678', '2', '2', '2019-06-21 19:06:24');
INSERT INTO `device_swing_card_record` VALUES ('477', '233114270', '1529331798', '1', '1', '2019-06-21 19:06:25');
INSERT INTO `device_swing_card_record` VALUES ('478', '233114270', '171200678', '2', '2', '2019-06-21 19:07:13');
INSERT INTO `device_swing_card_record` VALUES ('479', '233114270', '1529331798', '1', '1', '2019-06-21 19:07:14');
INSERT INTO `device_swing_card_record` VALUES ('480', '233114270', '171200678', '2', '2', '2019-06-21 19:07:24');
INSERT INTO `device_swing_card_record` VALUES ('481', '233114270', '1529331798', '1', '1', '2019-06-21 19:07:26');
INSERT INTO `device_swing_card_record` VALUES ('482', '233114270', '171200678', '2', '2', '2019-06-21 19:07:33');
INSERT INTO `device_swing_card_record` VALUES ('483', '233114270', '1529331798', '1', '1', '2019-06-21 19:07:34');
INSERT INTO `device_swing_card_record` VALUES ('484', '233114270', '171200678', '2', '2', '2019-06-21 19:07:38');
INSERT INTO `device_swing_card_record` VALUES ('485', '233114270', '1529331798', '1', '1', '2019-06-21 19:07:39');
INSERT INTO `device_swing_card_record` VALUES ('486', '233114270', '171200678', '2', '2', '2019-06-21 19:07:44');
INSERT INTO `device_swing_card_record` VALUES ('487', '233114270', '1529331798', '1', '1', '2019-06-21 19:07:45');
INSERT INTO `device_swing_card_record` VALUES ('488', '233114270', '171200678', '2', '2', '2019-06-21 19:07:49');
INSERT INTO `device_swing_card_record` VALUES ('489', '233114270', '1529331798', '1', '1', '2019-06-21 19:07:51');
INSERT INTO `device_swing_card_record` VALUES ('490', '233114270', '171200678', '2', '2', '2019-06-21 19:08:02');
INSERT INTO `device_swing_card_record` VALUES ('491', '233114270', '1529331798', '1', '1', '2019-06-21 19:08:05');
INSERT INTO `device_swing_card_record` VALUES ('492', '233114270', '171200678', '2', '2', '2019-06-21 19:10:22');
INSERT INTO `device_swing_card_record` VALUES ('493', '233114270', '1529331798', '1', '1', '2019-06-21 19:10:23');
INSERT INTO `device_swing_card_record` VALUES ('494', '233114270', '171200678', '2', '2', '2019-06-21 19:10:37');
INSERT INTO `device_swing_card_record` VALUES ('495', '233114270', '1529331798', '1', '1', '2019-06-21 19:10:39');
INSERT INTO `device_swing_card_record` VALUES ('496', '233114270', '171200678', '2', '2', '2019-06-21 19:11:00');
INSERT INTO `device_swing_card_record` VALUES ('497', '233114270', '1529331798', '1', '1', '2019-06-21 19:11:01');
INSERT INTO `device_swing_card_record` VALUES ('498', '233114270', '171200678', '2', '2', '2019-06-21 19:11:04');
INSERT INTO `device_swing_card_record` VALUES ('499', '233114270', '1529331798', '1', '1', '2019-06-21 19:11:05');
INSERT INTO `device_swing_card_record` VALUES ('500', '233114270', '171200678', '2', '2', '2019-06-21 19:11:37');
INSERT INTO `device_swing_card_record` VALUES ('501', '233114270', '1529331798', '1', '1', '2019-06-21 19:11:38');
INSERT INTO `device_swing_card_record` VALUES ('502', '233114270', '171200678', '2', '2', '2019-06-21 19:12:16');
INSERT INTO `device_swing_card_record` VALUES ('503', '233114270', '1529331798', '1', '1', '2019-06-21 19:12:17');
INSERT INTO `device_swing_card_record` VALUES ('504', '233114270', '171200678', '2', '2', '2019-06-21 19:12:19');
INSERT INTO `device_swing_card_record` VALUES ('505', '233114270', '1529331798', '1', '1', '2019-06-21 19:12:20');
INSERT INTO `device_swing_card_record` VALUES ('506', '233114270', '171200678', '2', '2', '2019-06-21 19:12:22');
INSERT INTO `device_swing_card_record` VALUES ('507', '233114270', '1529331798', '1', '1', '2019-06-21 19:12:23');
INSERT INTO `device_swing_card_record` VALUES ('508', '233114270', '1529331798', '1', '1', '2019-06-22 13:54:37');
INSERT INTO `device_swing_card_record` VALUES ('509', '233114270', '171200678', '2', '2', '2019-06-22 13:56:40');
INSERT INTO `device_swing_card_record` VALUES ('510', '233114270', '1529331798', '1', '1', '2019-06-22 13:56:42');
INSERT INTO `device_swing_card_record` VALUES ('511', '233114270', '171200678', '2', '2', '2019-06-22 13:56:53');
INSERT INTO `device_swing_card_record` VALUES ('512', '233114270', '1529331798', '1', '1', '2019-06-22 13:56:54');
INSERT INTO `device_swing_card_record` VALUES ('513', '233114270', '171200678', '2', '2', '2019-06-22 14:14:16');
INSERT INTO `device_swing_card_record` VALUES ('514', '233114270', '1529331798', '1', '1', '2019-06-22 14:14:17');
INSERT INTO `device_swing_card_record` VALUES ('515', '233114270', '171200678', '2', '2', '2019-06-22 14:14:30');
INSERT INTO `device_swing_card_record` VALUES ('516', '233114270', '171200678', '2', '2', '2019-06-22 14:14:41');
INSERT INTO `device_swing_card_record` VALUES ('517', '233114270', '1529331798', '1', '1', '2019-06-22 14:14:42');
INSERT INTO `device_swing_card_record` VALUES ('518', '233114270', '446797466', '2', '2', '2019-06-22 14:14:51');
INSERT INTO `device_swing_card_record` VALUES ('519', '233114270', '1529331798', '1', '1', '2019-06-22 14:14:52');
INSERT INTO `device_swing_card_record` VALUES ('520', '233114270', '171200678', '2', '2', '2019-06-22 14:15:58');
INSERT INTO `device_swing_card_record` VALUES ('521', '233114270', '1529331798', '1', '1', '2019-06-22 14:15:59');
INSERT INTO `device_swing_card_record` VALUES ('522', '233114270', '171200678', '2', '2', '2019-06-22 14:16:09');
INSERT INTO `device_swing_card_record` VALUES ('523', '233114270', '1529331798', '1', '1', '2019-06-22 14:16:11');
INSERT INTO `device_swing_card_record` VALUES ('524', '233114270', '446797466', '2', '2', '2019-06-22 14:16:15');
INSERT INTO `device_swing_card_record` VALUES ('525', '233114270', '446797466', '2', '2', '2019-06-22 14:18:52');
INSERT INTO `device_swing_card_record` VALUES ('526', '233114270', '171200678', '2', '2', '2019-06-22 14:19:04');
INSERT INTO `device_swing_card_record` VALUES ('527', '233114270', '1529331798', '1', '1', '2019-06-22 14:19:05');
INSERT INTO `device_swing_card_record` VALUES ('528', '233114270', '446797466', '2', '2', '2019-06-22 14:19:14');
INSERT INTO `device_swing_card_record` VALUES ('529', '233114270', '171200678', '2', '2', '2019-06-22 14:39:40');
INSERT INTO `device_swing_card_record` VALUES ('530', '233114270', '1529331798', '1', '1', '2019-06-22 14:39:41');
INSERT INTO `device_swing_card_record` VALUES ('531', '233114270', '171200678', '2', '2', '2019-06-22 14:39:51');
INSERT INTO `device_swing_card_record` VALUES ('532', '233114270', '171200678', '2', '2', '2019-06-22 14:40:13');
INSERT INTO `device_swing_card_record` VALUES ('533', '233114270', '171200678', '2', '2', '2019-06-22 14:40:23');
INSERT INTO `device_swing_card_record` VALUES ('534', '233114270', '1529331798', '1', '1', '2019-06-22 14:40:24');
INSERT INTO `device_swing_card_record` VALUES ('535', '233114270', '171200678', '2', '2', '2019-06-22 14:45:06');
INSERT INTO `device_swing_card_record` VALUES ('536', '233114270', '1529331798', '1', '1', '2019-06-22 14:45:07');
INSERT INTO `device_swing_card_record` VALUES ('537', '233114270', '171200678', '2', '2', '2019-06-22 14:46:33');
INSERT INTO `device_swing_card_record` VALUES ('538', '233114270', '1529331798', '1', '1', '2019-06-22 14:46:35');
INSERT INTO `device_swing_card_record` VALUES ('539', '233114270', '171200678', '2', '2', '2019-06-22 14:46:45');
INSERT INTO `device_swing_card_record` VALUES ('540', '233114270', '1529331798', '1', '1', '2019-06-22 14:46:46');
INSERT INTO `device_swing_card_record` VALUES ('541', '233114270', '171200678', '2', '2', '2019-06-22 14:49:21');
INSERT INTO `device_swing_card_record` VALUES ('542', '233114270', '1529331798', '1', '1', '2019-06-22 14:49:22');
INSERT INTO `device_swing_card_record` VALUES ('543', '233114270', '171200678', '2', '2', '2019-06-22 14:49:40');
INSERT INTO `device_swing_card_record` VALUES ('544', '233114270', '1529331798', '1', '1', '2019-06-22 14:49:41');
INSERT INTO `device_swing_card_record` VALUES ('545', '233114270', '171200678', '2', '2', '2019-06-22 14:49:57');
INSERT INTO `device_swing_card_record` VALUES ('546', '233114270', '1529331798', '1', '1', '2019-06-22 14:49:59');
INSERT INTO `device_swing_card_record` VALUES ('547', '233114270', '171200678', '2', '2', '2019-06-22 16:58:28');
INSERT INTO `device_swing_card_record` VALUES ('548', '233114270', '1529331798', '1', '1', '2019-06-22 16:58:29');
INSERT INTO `device_swing_card_record` VALUES ('549', '233114270', '718884262', '2', '2', '2019-06-22 17:00:41');
INSERT INTO `device_swing_card_record` VALUES ('550', '233114270', '446797466', '2', '2', '2019-06-22 17:00:47');
INSERT INTO `device_swing_card_record` VALUES ('551', '233114270', '446797466', '2', '2', '2019-06-22 17:04:21');
INSERT INTO `device_swing_card_record` VALUES ('552', '233114270', '446797466', '2', '2', '2019-06-30 15:28:13');
INSERT INTO `device_swing_card_record` VALUES ('553', '233114270', '171200678', '2', '2', '2019-06-30 15:33:18');
INSERT INTO `device_swing_card_record` VALUES ('554', '233114270', '1529331798', '1', '1', '2019-06-30 15:33:19');
INSERT INTO `device_swing_card_record` VALUES ('555', '233114270', '446797466', '2', '2', '2019-06-30 15:33:50');
INSERT INTO `device_swing_card_record` VALUES ('556', '233114270', '446797466', '2', '2', '2019-06-30 15:34:55');
INSERT INTO `device_swing_card_record` VALUES ('557', '233114270', '1529331798', '1', '1', '2019-06-30 15:34:56');
INSERT INTO `device_swing_card_record` VALUES ('558', '233114270', '718884262', '2', '2', '2019-06-30 15:35:07');
INSERT INTO `device_swing_card_record` VALUES ('559', '233114270', '171200678', '2', '2', '2019-06-30 15:46:05');
INSERT INTO `device_swing_card_record` VALUES ('560', '233114270', '1529331798', '1', '1', '2019-06-30 15:46:06');
INSERT INTO `device_swing_card_record` VALUES ('561', '233114270', '171200678', '2', '2', '2019-06-30 15:47:20');
INSERT INTO `device_swing_card_record` VALUES ('562', '233114270', '171200678', '2', '2', '2019-06-30 15:47:46');
INSERT INTO `device_swing_card_record` VALUES ('563', '233114270', '446797466', '2', '2', '2019-06-30 15:48:00');
INSERT INTO `device_swing_card_record` VALUES ('564', '233114270', '446797466', '2', '2', '2019-06-30 15:48:10');
INSERT INTO `device_swing_card_record` VALUES ('565', '233114270', '1529331798', '1', '1', '2019-06-30 15:48:11');
INSERT INTO `device_swing_card_record` VALUES ('566', '233114270', '718884262', '2', '2', '2019-06-30 15:48:20');
INSERT INTO `device_swing_card_record` VALUES ('567', '233114270', '718884262', '2', '2', '2019-06-30 15:48:34');
INSERT INTO `device_swing_card_record` VALUES ('568', '233114270', '1529331798', '1', '1', '2019-06-30 15:48:36');
INSERT INTO `device_swing_card_record` VALUES ('569', '233114270', '171200678', '2', '2', '2019-06-30 15:52:57');
INSERT INTO `device_swing_card_record` VALUES ('570', '233114270', '1529331798', '1', '1', '2019-06-30 15:52:58');
INSERT INTO `device_swing_card_record` VALUES ('571', '233114270', '171200678', '2', '2', '2019-06-30 15:53:25');
INSERT INTO `device_swing_card_record` VALUES ('572', '233114270', '1529331798', '1', '1', '2019-06-30 15:53:26');
INSERT INTO `device_swing_card_record` VALUES ('573', '233114270', '1529331798', '1', '1', '2019-07-02 09:59:49');
INSERT INTO `device_swing_card_record` VALUES ('574', '233114270', '171200678', '2', '2', '2019-07-02 09:59:57');
INSERT INTO `device_swing_card_record` VALUES ('575', '233114270', '171200678', '2', '2', '2019-07-02 10:00:52');
INSERT INTO `device_swing_card_record` VALUES ('576', '233114270', '446797466', '2', '2', '2019-07-02 10:01:39');
INSERT INTO `device_swing_card_record` VALUES ('577', '233114270', '171200678', '2', '2', '2019-07-02 10:04:25');
INSERT INTO `device_swing_card_record` VALUES ('578', '233114270', '171200678', '2', '2', '2019-07-03 15:44:56');
INSERT INTO `device_swing_card_record` VALUES ('579', '233114270', '2242736323', '2', '2', '2019-07-03 15:45:07');
INSERT INTO `device_swing_card_record` VALUES ('580', '233114270', '1529331798', '1', '1', '2019-07-03 15:45:11');
INSERT INTO `device_swing_card_record` VALUES ('581', '233114270', '2242736323', '2', '2', '2019-07-03 15:45:13');
INSERT INTO `device_swing_card_record` VALUES ('582', '233114270', '2242736323', '2', '2', '2019-07-03 15:45:21');
INSERT INTO `device_swing_card_record` VALUES ('583', '233114270', '718884262', '2', '2', '2019-07-03 16:00:05');
INSERT INTO `device_swing_card_record` VALUES ('584', '233114270', '2242736323', '2', '2', '2019-07-03 16:00:10');
INSERT INTO `device_swing_card_record` VALUES ('585', '233114270', '718884262', '2', '2', '2019-07-03 16:00:17');
INSERT INTO `device_swing_card_record` VALUES ('586', '233114270', '718884262', '2', '2', '2019-07-03 16:00:19');
INSERT INTO `device_swing_card_record` VALUES ('587', '233114270', '718884262', '2', '2', '2019-07-03 16:00:25');

-- ----------------------------
-- Table structure for `logger_logs`
-- ----------------------------
DROP TABLE IF EXISTS `logger_logs`;
CREATE TABLE `logger_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `service_name` varchar(20) DEFAULT NULL COMMENT '操作人ID（u_user）',
  `title` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '标题',
  `content` varchar(2000) CHARACTER SET utf8 DEFAULT NULL COMMENT '操作描述',
  `createDate` datetime DEFAULT NULL COMMENT '日志记录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COMMENT='系统日志表';

-- ----------------------------
-- Records of logger_logs
-- ----------------------------
INSERT INTO `logger_logs` VALUES ('12', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=171200678 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:33:17');
INSERT INTO `logger_logs` VALUES ('13', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-06-30 15:33:17');
INSERT INTO `logger_logs` VALUES ('14', 'service-control', '刷卡', '卡号:171200678 查询到员工 大海 所属组:员工组', '2019-06-30 15:33:17');
INSERT INTO `logger_logs` VALUES ('15', 'service-control', '刷卡', '卡号:171200678 开始校验 人员的凭证规则', '2019-06-30 15:33:17');
INSERT INTO `logger_logs` VALUES ('16', 'service-control', '刷卡', '卡号:171200678 校验 人员的凭证规则 通过', '2019-06-30 15:33:17');
INSERT INTO `logger_logs` VALUES ('17', 'service-control', '刷卡', '卡号:171200678 开始校验 人员的路径规则', '2019-06-30 15:33:17');
INSERT INTO `logger_logs` VALUES ('18', 'service-control', '刷卡', '卡号:171200678 校验 人员的所有规则全部通过,开门结果:{\"code\":1000,\"msg\":\"开门成功\"}', '2019-06-30 15:33:18');
INSERT INTO `logger_logs` VALUES ('19', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=446797466 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:33:50');
INSERT INTO `logger_logs` VALUES ('20', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-06-30 15:33:50');
INSERT INTO `logger_logs` VALUES ('21', 'service-control', '刷卡', '卡号:446797466 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:446797466 没有找到任何人员\"}', '2019-06-30 15:33:50');
INSERT INTO `logger_logs` VALUES ('22', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=446797466 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:34:55');
INSERT INTO `logger_logs` VALUES ('23', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-06-30 15:34:55');
INSERT INTO `logger_logs` VALUES ('24', 'service-control', '刷卡', '卡号:446797466 查询到访客 谢金发 所属组:缺省组', '2019-06-30 15:34:55');
INSERT INTO `logger_logs` VALUES ('25', 'service-control', '刷卡', '卡号:446797466 开始校验 人员的凭证规则', '2019-06-30 15:34:55');
INSERT INTO `logger_logs` VALUES ('26', 'service-control', '刷卡', '卡号:446797466 校验 人员的凭证规则 通过', '2019-06-30 15:34:55');
INSERT INTO `logger_logs` VALUES ('27', 'service-control', '刷卡', '卡号:446797466 开始校验 人员的路径规则', '2019-06-30 15:34:55');
INSERT INTO `logger_logs` VALUES ('28', 'service-control', '刷卡', '卡号:446797466 校验 人员的所有规则全部通过,开门结果:{\"code\":1000,\"msg\":\"开门成功\"}', '2019-06-30 15:34:56');
INSERT INTO `logger_logs` VALUES ('29', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=718884262 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:35:08');
INSERT INTO `logger_logs` VALUES ('30', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-06-30 15:35:08');
INSERT INTO `logger_logs` VALUES ('31', 'service-control', '刷卡', '卡号:718884262 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:718884262 没有找到任何人员\"}', '2019-06-30 15:35:08');
INSERT INTO `logger_logs` VALUES ('32', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=171200678 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:46:06');
INSERT INTO `logger_logs` VALUES ('33', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-06-30 15:46:06');
INSERT INTO `logger_logs` VALUES ('34', 'service-control', '刷卡', '卡号:171200678 查询到员工 大海 所属组:员工组', '2019-06-30 15:46:06');
INSERT INTO `logger_logs` VALUES ('35', 'service-control', '刷卡', '卡号:171200678 开始校验 人员的凭证规则', '2019-06-30 15:46:06');
INSERT INTO `logger_logs` VALUES ('36', 'service-control', '刷卡', '卡号:171200678 开始校验 人员的路径规则', '2019-06-30 15:46:06');
INSERT INTO `logger_logs` VALUES ('37', 'service-control', '刷卡', '卡号:171200678 校验 人员的凭证规则 通过', '2019-06-30 15:46:06');
INSERT INTO `logger_logs` VALUES ('38', 'service-control', '刷卡', '卡号:171200678 校验 人员的所有规则全部通过,开门结果:{\"code\":1000,\"msg\":\"开门成功\"}', '2019-06-30 15:46:06');
INSERT INTO `logger_logs` VALUES ('39', 'service-control', '刷卡', 'filter过滤器当前设置为:全部闸机关闭', '2019-06-30 15:47:21');
INSERT INTO `logger_logs` VALUES ('40', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=171200678 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:47:21');
INSERT INTO `logger_logs` VALUES ('41', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=171200678 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:47:47');
INSERT INTO `logger_logs` VALUES ('42', 'service-control', '刷卡', 'filter过滤器当前设置为:全部闸机只许进', '2019-06-30 15:47:47');
INSERT INTO `logger_logs` VALUES ('43', 'service-control', '刷卡', '卡号:171200678 是出，被禁止', '2019-06-30 15:47:47');
INSERT INTO `logger_logs` VALUES ('44', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=446797466 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:48:01');
INSERT INTO `logger_logs` VALUES ('45', 'service-control', '刷卡', 'filter过滤器当前设置为:全部闸机只许进', '2019-06-30 15:48:01');
INSERT INTO `logger_logs` VALUES ('46', 'service-control', '刷卡', '卡号:446797466 是出，被禁止', '2019-06-30 15:48:01');
INSERT INTO `logger_logs` VALUES ('47', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=446797466 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:48:10');
INSERT INTO `logger_logs` VALUES ('48', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-06-30 15:48:10');
INSERT INTO `logger_logs` VALUES ('49', 'service-control', '刷卡', '卡号:446797466 查询到访客 谢金发 所属组:缺省组', '2019-06-30 15:48:10');
INSERT INTO `logger_logs` VALUES ('50', 'service-control', '刷卡', '卡号:446797466 开始校验 人员的凭证规则', '2019-06-30 15:48:10');
INSERT INTO `logger_logs` VALUES ('51', 'service-control', '刷卡', '卡号:446797466 校验 人员的凭证规则 通过', '2019-06-30 15:48:10');
INSERT INTO `logger_logs` VALUES ('52', 'service-control', '刷卡', '卡号:446797466 开始校验 人员的路径规则', '2019-06-30 15:48:10');
INSERT INTO `logger_logs` VALUES ('53', 'service-control', '刷卡', '卡号:446797466 校验 人员的所有规则全部通过,开门结果:{\"code\":1000,\"msg\":\"开门成功\"}', '2019-06-30 15:48:10');
INSERT INTO `logger_logs` VALUES ('54', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=718884262 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:48:20');
INSERT INTO `logger_logs` VALUES ('55', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-06-30 15:48:20');
INSERT INTO `logger_logs` VALUES ('56', 'service-control', '刷卡', '卡号:718884262 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:718884262 没有找到任何人员\"}', '2019-06-30 15:48:20');
INSERT INTO `logger_logs` VALUES ('57', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=718884262 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:48:34');
INSERT INTO `logger_logs` VALUES ('58', 'service-control', '刷卡', 'filter过滤器当前设置为:全部闸机常开', '2019-06-30 15:48:34');
INSERT INTO `logger_logs` VALUES ('59', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=171200678 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:52:58');
INSERT INTO `logger_logs` VALUES ('60', 'service-control', '刷卡', 'filter过滤器当前设置为:全部闸机常开', '2019-06-30 15:52:58');
INSERT INTO `logger_logs` VALUES ('61', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=171200678 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-06-30 15:53:26');
INSERT INTO `logger_logs` VALUES ('62', 'service-control', '刷卡', 'filter过滤器当前设置为:全部闸机常开', '2019-06-30 15:53:26');
INSERT INTO `logger_logs` VALUES ('63', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=171200678 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 15:44:45');
INSERT INTO `logger_logs` VALUES ('64', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 15:44:47');
INSERT INTO `logger_logs` VALUES ('65', 'service-control', '刷卡', '卡号:171200678 查询到员工 大海 所属组:员工组', '2019-07-03 15:44:49');
INSERT INTO `logger_logs` VALUES ('66', 'service-control', '刷卡', '卡号:171200678 开始校验 人员的凭证规则', '2019-07-03 15:44:49');
INSERT INTO `logger_logs` VALUES ('67', 'service-control', '刷卡', '卡号:171200678 校验 人员的凭证规则 通过', '2019-07-03 15:44:49');
INSERT INTO `logger_logs` VALUES ('68', 'service-control', '刷卡', '卡号:171200678 开始校验 人员的路径规则', '2019-07-03 15:44:49');
INSERT INTO `logger_logs` VALUES ('69', 'service-control', '刷卡', '卡号:171200678 校验 人员的所有规则全部通过,开门结果:{\"code\":1000,\"msg\":\"开门成功\"}', '2019-07-03 15:44:57');
INSERT INTO `logger_logs` VALUES ('70', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=2242736323 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 15:45:07');
INSERT INTO `logger_logs` VALUES ('71', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 15:45:07');
INSERT INTO `logger_logs` VALUES ('72', 'service-control', '刷卡', '卡号:2242736323 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:2242736323 没有找到任何人员\"}', '2019-07-03 15:45:07');
INSERT INTO `logger_logs` VALUES ('73', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=2242736323 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 15:45:14');
INSERT INTO `logger_logs` VALUES ('74', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 15:45:14');
INSERT INTO `logger_logs` VALUES ('75', 'service-control', '刷卡', '卡号:2242736323 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:2242736323 没有找到任何人员\"}', '2019-07-03 15:45:14');
INSERT INTO `logger_logs` VALUES ('76', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=2242736323 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 15:45:22');
INSERT INTO `logger_logs` VALUES ('77', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 15:45:22');
INSERT INTO `logger_logs` VALUES ('78', 'service-control', '刷卡', '卡号:2242736323 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:2242736323 没有找到任何人员\"}', '2019-07-03 15:45:22');
INSERT INTO `logger_logs` VALUES ('79', 'service-facedevice', '22', '大事', '2019-07-03 15:46:02');
INSERT INTO `logger_logs` VALUES ('80', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=718884262 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 16:00:05');
INSERT INTO `logger_logs` VALUES ('81', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 16:00:05');
INSERT INTO `logger_logs` VALUES ('82', 'service-control', '刷卡', '卡号:718884262 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:718884262 没有找到任何人员\"}', '2019-07-03 16:00:05');
INSERT INTO `logger_logs` VALUES ('83', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=2242736323 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 16:00:10');
INSERT INTO `logger_logs` VALUES ('84', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 16:00:10');
INSERT INTO `logger_logs` VALUES ('85', 'service-control', '刷卡', '卡号:2242736323 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:2242736323 没有找到任何人员\"}', '2019-07-03 16:00:10');
INSERT INTO `logger_logs` VALUES ('86', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 16:00:18');
INSERT INTO `logger_logs` VALUES ('87', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=718884262 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 16:00:18');
INSERT INTO `logger_logs` VALUES ('88', 'service-control', '刷卡', '卡号:718884262 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:718884262 没有找到任何人员\"}', '2019-07-03 16:00:18');
INSERT INTO `logger_logs` VALUES ('89', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 16:00:20');
INSERT INTO `logger_logs` VALUES ('90', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=718884262 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 16:00:20');
INSERT INTO `logger_logs` VALUES ('91', 'service-control', '刷卡', '卡号:718884262 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:718884262 没有找到任何人员\"}', '2019-07-03 16:00:20');
INSERT INTO `logger_logs` VALUES ('92', 'service-control', '刷卡', '接收到:sn=233114270 cardNum=718884262 doorNo=2 inOrOut=2 (1:进 2:出)', '2019-07-03 16:00:26');
INSERT INTO `logger_logs` VALUES ('93', 'service-control', '刷卡', 'filter过滤器当前设置为:无限制', '2019-07-03 16:00:26');
INSERT INTO `logger_logs` VALUES ('94', 'service-control', '刷卡', '卡号:718884262 查找人员失败，结果:{\"code\":4000,\"msg\":\"根据卡号:718884262 没有找到任何人员\"}', '2019-07-03 16:00:26');
DROP TRIGGER IF EXISTS `trigger_member_insert`;
DELIMITER ;;
CREATE TRIGGER `trigger_member_insert` BEFORE INSERT ON `base_member` FOR EACH ROW begin
DECLARE tempPhone VARCHAR(20) ;
if(new.phone like '90%' and LENGTH(new.phone > 11))
then
set new.phone = substring(new.phone, 3) ;
end if;
end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `trigger_member_update`;
DELIMITER ;;
CREATE TRIGGER `trigger_member_update` BEFORE UPDATE ON `base_member` FOR EACH ROW begin
if(new.phone like '90%' and LENGTH(new.phone > 11))
then
set new.phone = substring(new.phone, 3) ;
end if;
end
;;
DELIMITER ;
